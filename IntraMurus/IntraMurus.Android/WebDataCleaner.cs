﻿using Android.Webkit;
using IntraMurus.Droid;
using IntraMurus.Network.Helpers;

[assembly: Xamarin.Forms.Dependency(typeof(WebDataCleaner))]
namespace IntraMurus.Droid
{
    public class WebDataCleaner : IWebDataCleaner
    {
        public void ClearData()
        {
            WebStorage.Instance.DeleteAllData();
            CookieManager.Instance.RemoveAllCookie();
        }
    }
}