using IntraMurus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using IntraMurus.Models;
using IntraMurus.Network;
using IntraMurus.Network.Request;
using System.Threading;
using IntraMurus.Network.Response;
using Xamarin.Forms.Internals;
using IntraMurus.ViewModels.Base;

namespace IntraMurus.ViewModels
{
    public class InscriptionViewModel : BaseViewModel
    {
        //constants
        public const int MIN_PLAYERS = 0;
        public const string API_URL = "api/equipes/createEquipe";
        public const string API_URL_GET_SPORTS = "api/filtres/sports";
        public const string API_URL_GET_LIGUES_BY_SPORT = "api/filtres/ligues";
        public const string CACHE_KEY = "UncompletedInscription";

        //private vars
        private string _cipentry;
        private string _sportSelected;
        private int _minNumPlayers;
        private string _categorySelected;
        private string _teamName;
        private bool _listIsVisible;           
        private Regex rgx = new Regex(@"^[a-z]{4}[0-9]{4}");
        private Inscription inscription;
        private ObservableCollection<Player> initialCIPList;
        private ObservableCollection<Player> temp;
        private Student _studentSelected;
        private int searchCounter;

        private readonly IPageService _pageservice;
        private ISportsService _sportService;
        private ISportsCategoriesService _sportsCategoriesService;
        private IMemoryCacheService _myCacheService;
        private INetworkService _networkService;
        private IUdeSService _udesService;


        public ObservableCollection<string> Sports { get; private set; }
        public ObservableCollection<string> Categories { get; private set; }
        public ObservableCollection<Player> InsPlayers { get; private set; }

        public ObservableCollection<Student> SearchStudents { get; private set; }

        private object listViewCIP;

        //commands
        public ICommand AddButtonClickedCommand { get; private set; }
        public ICommand CIPListSearchCommand { get; private set; }
        public ICommand RegisterButtonClickedCommand { get; private set; }
        public ICommand RemovePlayerFromListCommand { get; private set; }
        public ICommand OnAppearingCommandList { get; private set; }
        public ICommand OnAppearingCommandCache { get; private set; }
        public ICommand OnDisappearingCommand { get; private set; }
        public ICommand SportPickerCommand { get; private set; }
        public ICommand EntryChangedCommand { get; private set; }
        public ICommand SelectedStudentFromListCommand { get; private set; }


        //constructor
        public InscriptionViewModel(IPageService pageservice, ISportsService sportsService, ISportsCategoriesService sportsCategoriesService,
                                    IMemoryCacheService memoryCacheService, INetworkService networkService, IUdeSService udesService)
        {
            _pageservice = pageservice;
            _sportService = sportsService;
            _sportsCategoriesService = sportsCategoriesService;
            _myCacheService = memoryCacheService;
            _networkService = networkService;
            _udesService = udesService;

            //display initialization
            Sports = new ObservableCollection<string>();
            InsPlayers = new ObservableCollection<Player>();
            Categories = new ObservableCollection<string>();
            inscription = new Inscription();
            SearchStudents = new ObservableCollection<Student>();

            //commands
            OnAppearingCommandList = new Command(async () => await GetAllSports());
            OnAppearingCommandCache = new Command(GetCacheContent);
            OnDisappearingCommand = new Command(SetCacheContent);
            AddButtonClickedCommand = new Command(AddButtonClicked);
            CIPListSearchCommand = new Command<string>(Handle_ListSearch);
            RegisterButtonClickedCommand = new Command(async () => await RegisterButtonClicked());
            RemovePlayerFromListCommand = new Command<Player>(RemovePlayerFromList);
            SportPickerCommand = new Command(async () => await HandleSportPicked());
            EntryChangedCommand = new Command<string>(HandleEntryChanged);
            SelectedStudentFromListCommand = new Command(HandleStudentSelected);
            initialCIPList = null;
            searchCounter = 0;
        }

        //bindings
        public string CIPEntry
        {
            get { return _cipentry; }
            set { SetValue(ref _cipentry, value); }
        }

        public string SportSelected
        {
            get { return _sportSelected; }
            set
            {
                SetValue(ref _sportSelected, value);

            }
        }

        public bool VisibleList
        {
            get { return _listIsVisible; }
            set
            {
                SetValue(ref _listIsVisible, value);

            }
        }

        public string CategorySelected
        {
            get { return _categorySelected; }
            set { SetValue(ref _categorySelected, value);
            }

        }

        public int MinNumPlayers
        {
            get { return _minNumPlayers; }
            set { SetValue(ref _minNumPlayers, value); }
        }

        public string TeamName
        {
            get { return _teamName; }
            set { SetValue(ref _teamName, value);

            }
        }

        public Student StudentSelected
        {
            get { return _studentSelected; }
            set { SetValue(ref _studentSelected, value); }   
        }

        //methods
        private void AddButtonClicked()
        {
            VisibleList = false;
            searchCounter = 0;
            //check regex
            if (_cipentry != null && ValidateRegex(_cipentry, rgx) == true)
            {
                if (NoDoubles(_cipentry) == true)
                {
                    //can add to observable collection
                    InsPlayers.Add(new Player
                    {
                        Name = "",  //eventually calls LDAP service to get the name using the _cipentry
                        CIP = _cipentry,
                        Status = Availability.None

                    });
                    CIPEntry = "";

                    //clear the listView of the cip entry
                    SearchStudents.Clear();
                }
                else
                {
                    _pageservice.DisplayAlert("Alerte", "CIP d�j� dans la liste des joueurs.", "OK");
                }
            }
            else
            {
                _pageservice.DisplayAlert("Alerte", "CIP invalide.", "OK");
            }

        }

        private void RemovePlayerFromList(Player player)
        {
            InsPlayers.Remove(player);
            if (initialCIPList != null && initialCIPList.Contains(player))
            {
                initialCIPList.Remove(player);
            }
            if (temp != null && temp.Contains(player))
            {
                temp.Remove(player);
            }
         
        }

        private void Handle_ListSearch(string strSearch)
        {
            
            if (searchCounter == 0)
            {
                initialCIPList = new ObservableCollection<Player>(InsPlayers);
                searchCounter++;
            }

            if (InsPlayers.Count() > 0 && !string.IsNullOrWhiteSpace(strSearch))
            {
                IEnumerable<Player> playersToKeep;
                playersToKeep = InsPlayers.Where(p => (p.CIP.StartsWith(strSearch)));
                temp = new ObservableCollection<Player>(playersToKeep);
                InsPlayers.Clear();
                temp.ForEach(ip => InsPlayers.Add(ip));
            }
            
            if (InsPlayers.Count() == 0 && !string.IsNullOrWhiteSpace(strSearch) && initialCIPList.Count() > 0)
            {
                IEnumerable<Player> playersToRestore;
                playersToRestore = initialCIPList.Where(p => (p.CIP.StartsWith(strSearch)));
                if (playersToRestore.Count() > 0)
                {
                    playersToRestore.ForEach(ip => InsPlayers.Add(ip));
                }
            }

            if (initialCIPList != null && string.IsNullOrWhiteSpace(strSearch))
            {
                InsPlayers.Clear();
                initialCIPList.ForEach(ip => InsPlayers.Add(ip));             
            }
        }

        public void Handle_SportChanged()
        {
            _minNumPlayers = MIN_PLAYERS;
            //set catergories TODO
            _sportSelected = _sportSelected.Replace(" ", "_");
            Categories = _sportsCategoriesService.GetSportCategories(_sportSelected);
            emptyCIPEntryField();
        }

        private async Task HandleSportPicked()
        {
            try
            {
                if (SportSelected != null)
                {
                    var token = new CancellationToken();
                    var response = await _networkService.GetListAsync<string>($"api/filtres/ligues?sport={SportSelected.Replace(' ', '_')}", token);
                    CastStringToObservableCollection(response, Categories);
                }      
            }
            catch (Exception)
            {
                await _pageservice.DisplayAlert("Alerte serveur", "Serveur indisponible. Veuillez r�essayez plus tard.", "OK");
            }
        }


        private async Task RegisterButtonClicked()
        {
            //get listView values 
            if (_teamName == null || string.IsNullOrWhiteSpace(_teamName))
            {
                await _pageservice.DisplayAlert("Erreur d'inscription", "Veuillez d'abord choisir un nom d'�quipe", "OK");
            }
            else if (_sportSelected == null)
            {
                await _pageservice.DisplayAlert("Erreur d'inscription", "Veuillez choisir un sport", "OK");
            }
            else if (_categorySelected == null)
            {
                await _pageservice.DisplayAlert("Erreur d'inscription", "Veuillez choisir une cat�gorie", "OK");
            }
            else
            {
                if (InsPlayers.Count >= MIN_PLAYERS)
                {

                    try
                    {
                       // SetInscriptionObjectVariables();

                        //create a new inscription object and fill it 
                        inscription = new Inscription();
                        inscription.SportName = SportSelected;
                        inscription.Category = CategorySelected;
                        inscription.PlayersList = InsPlayers;
                        inscription.TeamName = TeamName;

                        var playersString = GetPlayerListAsString();

                        PostTeamCreationRequest Treq = new PostTeamCreationRequest();
                        SetTeamCreationAPIRequest(Treq);
                        var token = new CancellationToken();
                        var response = await _networkService.PostAsync<PostTeamCreationRequest, BaseResponse>(API_URL, Treq, token);                   
                        await _pageservice.DisplayAlert("Inscription valide", "Votre �quipe a �t� soumise en approbation. Merci.", "OK");                       
                    }
                    catch (ApiException e) when (e.StatusCode == ApiStatus.TeamInscriptionError)
                    {
                        await _pageservice.DisplayAlert("Inscription invalide", 
                            "Impossible de faire partie de deux �quipes dans la m�me ligue.", "OK");
                    }
                    catch (Exception)
                    {
                        await _pageservice.DisplayAlert("Inscription invalide", "Une erreur s'est produite.", "OK");
                    } 
                }
                else
                {
                    await _pageservice.DisplayAlert("Erreur d'inscription", "Joueurs insuffisants. Le nombre de joueurs requis est de" + _minNumPlayers, "OK");
                }
            }
            
        }
           
        private bool ValidateRegex(string str, Regex regex)
        {
            if (regex.IsMatch(str))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool NoDoubles(string cip)
        {    
            for (int i = 0; i < InsPlayers.Count; i++)
            {
                if (InsPlayers[i].CIP == cip)
                {
                    return false;               
                }
            }
            return true;
        }

        public List<string> DisplayAllSports()
        {
            return _sportService.GetAllSports();
        }

        private async Task GetAllSports()
        {
            try
            {
                var token = new CancellationToken();
                var response = await _networkService.GetListAsync<string>(API_URL_GET_SPORTS, token);
                CastStringToObservableCollection(response, Sports);          
            }
            catch (Exception)
            {
                await _pageservice.DisplayAlert("Alerte serveur", "Serveur indisponible. Veuillez r�essayez plus tard.", "OK");
            } 
        }

        private void HandleEntryChanged(string newValue)
        {
            //clear the listView of the previous entry
            if (SearchStudents.Count() > 0)
            {
                SearchStudents.Clear();
            }       

            Collection<Student> studentsList = new Collection<Student>();
            if (string.IsNullOrWhiteSpace(newValue) == false)
            {
                studentsList = _udesService.GetAllStudentsInfo();
                //todo: implement logic with newValue and set collection data to a listView

                studentsList.ForEach(s => SearchStudents.Add(s));
                VisibleList = true;
            }
            
        }

        private void HandleStudentSelected()
        {
            //make list not visible
            

            //set entry string 
            _cipentry = _studentSelected.Cip_etudiant;
            VisibleList = false;
        }

        private void CastStringToObservableCollection(List<string> strList, ObservableCollection<string> myOBC)
        {
            myOBC.Clear();
            for (int i = 0; i < strList.Count(); i++)
            {
                if (strList[i].Contains("_"))
                {
                    strList[i] = strList[i].Replace("_", " ");
                }
                myOBC.Add(strList[i]);
            }
        }


        private void GetCacheContent()
        {
            inscription = new Inscription();

            inscription = _myCacheService.Get<Inscription>(CACHE_KEY);
            if (inscription != null)
            {
                
                //set values binded to UI
                if (inscription.TeamName != null)
                {
                    TeamName = inscription.TeamName;
                }
                if (inscription.SportName != null)
                {
                    SportSelected = inscription.SportName;
                }
                if (inscription.Category != null)
                {
                    CategorySelected = inscription.Category;
                }
                if (inscription.PlayersList != null)
                {
                    InsPlayers.Clear();
                    inscription.PlayersList.ForEach(p => InsPlayers.Add(p));
                }       
            }       
        }

        private void SetCacheContent()
        {
            inscription = new Inscription();
            //fill up inscription object
            inscription.TeamName = TeamName;
            inscription.Category = CategorySelected;
            inscription.SportName = SportSelected;
            inscription.PlayersList = InsPlayers;
            //TODO: Get dynamic expiry dates 
            DateTimeOffset absoluteExpiry = new DateTimeOffset(2018, 12, 1, 8, 6, 32, new TimeSpan(1, 0, 0));
            _myCacheService.Set(CACHE_KEY, inscription, absoluteExpiry);
        }

        private string GetPlayerListAsString()
        {
            string playersString = "";
            inscription.PlayersList.ForEach(p => playersString += $"{p.CIP},");
            playersString = playersString.Remove(playersString.Length - 1);
            return playersString;
        }

        private void SetTeamCreationAPIRequest( PostTeamCreationRequest Treq)
        {   
            Treq.cipCapitaine = (Application.Current as App).UserDetails.Cip;
            Treq.cipsJoueurs = GetPlayerListAsString();
            Treq.nomLigue = inscription.Category;
            Treq.nomEquipe = inscription.TeamName;
            Treq.sport = inscription.SportName.ToUpper();
        }

        public void emptyCIPEntryField()
        {
            CIPEntry = "";
        }

        public void SetInscriptionObjectVariables()
        {
            inscription.TeamName = TeamName;
            inscription.Category = CategorySelected;
            inscription.SportName = SportSelected;
            inscription.PlayersList = InsPlayers;
        }

    }
   
}
