﻿using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using IntraMurus.Views;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class MenuPageViewModel : BaseViewModel
    {
        private IMasterNavigationService _masterNavigationService;
        private INetworkService _networkService;

        public ICommand OpenHomeCommand => new Command(async () => await OpenHome());

        public ICommand OpenScheduleCommand => new Command(async () => await OpenSchedule());

        public ICommand OpenBoardCommand => new Command(async () => await OpenBoard());

        public ICommand OpenTeamCreationCommand => new Command(async () => await OpenTeamCreation());

        public ICommand OpenMyTeamsCommand => new Command(async () => await OpenMyTeams());

        public ICommand OpenNewConnectionCommand => new Command(OpenNewConnection);

        public MenuPageViewModel(IMasterNavigationService masterNavigationService, INetworkService networkService)
        {
            _masterNavigationService = masterNavigationService;
            _networkService = networkService;
        }

        private async Task OpenHome()
        {
            await _masterNavigationService.NavigateToPage(new HomePage());
        }

        private async Task OpenSchedule()
        {
            await _masterNavigationService.NavigateToPage(new SchedulePage());
        }

        private async Task OpenBoard()
        {
            await _masterNavigationService.NavigateToPage(new BoardPage());
        }

        private async Task OpenTeamCreation()
        {
            await _masterNavigationService.PushAsync(new InscriptionPage());
        }

        private async Task OpenMyTeams()
        {
            await _masterNavigationService.PushAsync(new MyTeamsPage());
        }

        private void OpenNewConnection()
        {
            _networkService.SetToken(string.Empty);
            _masterNavigationService.SetMainPage(new AuthentificationPage());
        }
    }
}
