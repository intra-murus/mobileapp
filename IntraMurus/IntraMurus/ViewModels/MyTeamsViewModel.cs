﻿using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using IntraMurus.ViewModels.Grouping;
using IntraMurus.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace IntraMurus.ViewModels
{
    public class MyTeamsViewModel : CancellableTaskViewModel
    {
        private DetailedTeamViewModel _selectedTeam;
        private IUserService _userService;
        private IMasterNavigationService _masterNavigationService;
        private ITeamService _teamService;
        private bool _isRefreshing;
        private string _emptyListMessage;

        public ObservableCollection<InscriptionStatusGroup> MyTeams { get; private set; }

        public DetailedTeamViewModel SelectedTeam
        {
            get { return _selectedTeam; }
            set { SetValue(ref _selectedTeam, value); }
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetValue(ref _isRefreshing, value); }
        }

        public string EmptyListMessage
        {
            get { return _emptyListMessage; }
            set { SetValue(ref _emptyListMessage, value); }
        }

        public ICommand UpdateTeamsCommand => new Command(async () => await UpdateTeams());

        public ICommand DisplaySelectedTeamPageCommand => new Command<DetailedTeamViewModel>(async (t) => await OpenDetailedTeamPage(t));

        public MyTeamsViewModel(IUserService userService, IMasterNavigationService masterNavigationService, ITeamService teamService)
        {
            _userService = userService;
            _masterNavigationService = masterNavigationService;
            _teamService = teamService;
            MyTeams = new ObservableCollection<InscriptionStatusGroup>();
        }

        private async Task UpdateTeams()
        {
            try
            {
                await _uniqueExecutionTaskQueue.Execute(async (token) =>
                {
                    token.ThrowIfCancellationRequested();
                    IsRefreshing = true;

                    var teams = await _userService.GetUserTeams(token);
                    token.ThrowIfCancellationRequested();

                    MyTeams.Clear();
                    teams.Sort();
                    var lookupTable = teams.ToLookup(t => t.Status, t => t);

                    foreach (var groupItem in lookupTable)
                    {
                        var inscriptionStatusGroup = new InscriptionStatusGroup();
                        groupItem.ForEach(item => inscriptionStatusGroup.Add(new DetailedTeamViewModel(item, _teamService)));
                        MyTeams.Add(inscriptionStatusGroup);
                    }

                    EmptyListMessage = "Aucune équipe à afficher";
                });
            }
            catch (Exception)
            {
                EmptyListMessage = "Erreur lors du téléchargement des équipes";
            }
            finally
            {
                if (!_uniqueExecutionTaskQueue.IsPending())
                {
                    IsRefreshing = false;
                }
            }
        }

        private async Task OpenDetailedTeamPage(DetailedTeamViewModel detailedTeamViewModel)
        {
            if (detailedTeamViewModel == null)
                return;

            try
            {
                await _uniqueExecutionTaskQueue.Execute(async (token) =>
                {
                    var players = await _teamService.GetTeamMembers(detailedTeamViewModel.Team.TeamId, token);
                    var userCip = (Application.Current as App).UserDetails.Cip;

                    var user = players.RemoveAll(p => p.CIP == userCip);
                    detailedTeamViewModel.Team.InscriptionData.PlayersList = players;
                    detailedTeamViewModel.IsCaptain = detailedTeamViewModel.Team.CipCapitaine == userCip;
                });
                await _masterNavigationService.PushAsync(new DetailedTeamPage(detailedTeamViewModel));
            }
            catch (Exception)
            {

            }
            finally
            {
                SelectedTeam = null;
            }
        }
    }
}
