﻿using IntraMurus.Helpers;
using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class DetailedActivityViewModel : CancellableTaskViewModel
    {
        private CancellationTokenSource _cancellationTokenSource;
        private IActivityService _activityService;
        private IPageService _pageService;
        private string _timeLeftToConfirm;
        private bool _isConfirmationEnabled;

        public Activity Activity { get; set; }

        public Availability Status
        {
            get { return Activity.Status; }
            set { SetPropertyValue(Activity, value); }
        }

        public Availability TeamStatus
        {
            get { return Activity.TeamStatus; }
            set { SetPropertyValue(Activity, value); }
        }

        public string FormatedDate => DateTimeFormatHelper.GetFormatedDate(Activity.Date);

        public string FormatedTimeSpan => DateTimeFormatHelper.GetFormatedTimeSpan(Activity.Date, Activity.Duration);

        public string TimeLeftToConfirm
        {
            get { return _timeLeftToConfirm; }
            set { SetValue(ref _timeLeftToConfirm, value); }
        }

        public bool IsConfirmationEnabled
        {
            get { return _isConfirmationEnabled; }
            set { SetValue(ref _isConfirmationEnabled, value); }
        }

        public bool IsTeamCaptain => Activity.TeamCaptainCip == (Application.Current as App).UserDetails.Cip;

        public ICommand UserPresenceSelectionCommand => new Command<Availability>(async a => await UserPresenceSelection(a));

        public ICommand TeamPresenceSelectionCommand => new Command<Availability>(async a => await TeamPresenceSelection(a));

        public ICommand OnAppearingCommand => new Command(StartConfirmationTimer);

        public ICommand OnDisappearingCommand => new Command(StopConfirmationTimer);

        public DetailedActivityViewModel(Activity activity, IActivityService activityService, IPageService pageService)
        {
            Activity = activity;
            _activityService = activityService;
            _pageService = pageService;
            _cancellationTokenSource = new CancellationTokenSource();
            _uniqueExecutionTaskQueue = new UniqueExecutionTaskQueue();
        }

        private async Task UserPresenceSelection(Availability availability)
        {
            await PresenceSelection(async () =>
            {
                var oldStatus = Status;
                Status = availability;

                try
                {
                    await _uniqueExecutionTaskQueue.Execute(async (token) =>
                    {
                        await _activityService.UpdatePlayerStatus(Activity, token);
                    }, false);
                }
                catch (Exception)
                {
                    Status = oldStatus;
                }
            });
        }

        private async Task TeamPresenceSelection(Availability availability)
        {
            await PresenceSelection(async () =>
            {
                var oldStatus = TeamStatus;
                TeamStatus = availability;

                try
                {
                    await _uniqueExecutionTaskQueue.Execute(async (token) =>
                    {
                        await _activityService.UpdateTeamStatus(Activity, token);
                    }, false);
                }
                catch (Exception)
                {
                    TeamStatus = oldStatus;
                }
            });
        }

        private async Task PresenceSelection(Func<Task> setStatus)
        {
            if (IsConfirmationEnabled)
            {
                await setStatus();
            }
            else if (Status == Availability.None)
            {
                await _pageService.DisplayAlert("Confirmation de présence", "Impossible de modifier la confirmation de présence car le délais n'a pas encore débuté", "OK");
            }
            else
            {
                await _pageService.DisplayAlert("Confirmation de présence", "Impossible de modifier la confirmation de présence car le délais est expiré", "OK");
            }
        }

        private void StartConfirmationTimer()
        {
            SetTimeLeftToConfirm();

            if (Activity.ConfirmationLimit > DateTime.Now)
            {
                CancellationTokenSource cts = _cancellationTokenSource;

                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    if (cts.IsCancellationRequested)
                        return false;

                    Device.BeginInvokeOnMainThread(() => SetTimeLeftToConfirm());

                    return IsConfirmationEnabled;
                });
            }
        }

        private void StopConfirmationTimer()
        {
            Interlocked.Exchange(ref _cancellationTokenSource, new CancellationTokenSource()).Cancel();
        }

        private void SetTimeLeftToConfirm()
        {
            if (Activity.ConfirmationLimit < DateTime.Now)
            {
                TimeLeftToConfirm = "0:00:00";
                IsConfirmationEnabled = false;
            }
            else
            {
                var timeLeft = Activity.ConfirmationLimit - DateTime.Now;
                var hours = (int)timeLeft.TotalHours;
                TimeLeftToConfirm = $"{hours}:{timeLeft.ToString("mm':'ss")}";
                IsConfirmationEnabled = true;
            }
        }
    }
}
