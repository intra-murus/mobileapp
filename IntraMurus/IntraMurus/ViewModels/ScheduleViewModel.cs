﻿using IntraMurus.Helpers;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class ScheduleViewModel : CancellableTaskViewModel
    {
        private IScheduleService _scheduleService;
        private DateTime _monthOfYear;
        private string _formatedMonthOfYear;

        public ActivityListViewModel ActivityListViewModel { get; private set; }

        public string FormatedMonthOfYear
        {
            get { return _formatedMonthOfYear; }
            set { SetValue(ref _formatedMonthOfYear, value); }
        }

        public ICommand PreviousMonthCommand => new Command(async () => await PreviousMonth());

        public ICommand NextMonthCommand => new Command(async () => await NextMonth());

        public ICommand UpdateActivityScheduleCommand => new Command(async () => await UpdateActivitySchedule());

        public ScheduleViewModel(IScheduleService scheduleService, IActivityService activityService,
            IUserService userService, IMasterNavigationService masterNavigationService)
        {
            ActivityListViewModel = new ActivityListViewModel(scheduleService, activityService, userService, masterNavigationService, _uniqueExecutionTaskQueue);
            _scheduleService = scheduleService;
            _monthOfYear = DateTime.Now;
        }

        private async Task PreviousMonth()
        {
            _monthOfYear = _monthOfYear.AddMonths(-1);
            await UpdateActivitySchedule();
        }

        private async Task NextMonth()
        {
            _monthOfYear = _monthOfYear.AddMonths(1);
            await UpdateActivitySchedule();
        }

        protected async Task UpdateActivitySchedule()
        {
            FormatedMonthOfYear = DateTimeFormatHelper.GetFormatedMonthOfYear(_monthOfYear);
            await _uniqueExecutionTaskQueue.Execute(async (token) =>
            {
                await ActivityListViewModel.UpdateActivitySchedule(async () =>
                {
                    return await _scheduleService.GetActivitiesByMonthOfYear(_monthOfYear.Month, _monthOfYear.Year, token);
                }
                , token);
            });
        }
    }
}
