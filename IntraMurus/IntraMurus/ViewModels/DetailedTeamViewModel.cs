﻿using IntraMurus.Helpers;
using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class DetailedTeamViewModel : CancellableTaskViewModel
    {
        private ITeamService _teamService;

        public Team Team { get; set; }

        public bool IsCaptain { get; set; }

        public string FormatedSport => $"{Team.InscriptionData.SportName} {Team.InscriptionData.Category}";

        public string FormatedPeriod => LanguageConverter.PeriodTranslation(Team.InscriptionData.Period);

        public bool IsApprobationButtonVisible => IsCaptain && (Status == Approbation.Unspecified || Status == Approbation.Waiting);

        public Approbation Status
        {
            get { return Team.Status; }
            set { SetPropertyValue(Team, value); }
        }

        public ICommand ApprobationButtonClickedCommand => new Command(async () => await ApprobationButtonClicked());

        public DetailedTeamViewModel(Team team, ITeamService teamService)
        {
            Team = team;
            _teamService = teamService;
        }

        private async Task ApprobationButtonClicked()
        {
            Status = Approbation.Waiting;

            try
            {
                await _uniqueExecutionTaskQueue.Execute(async (token) =>
                {
                    await _teamService.PutStatutApprobation(Team.TeamId, token);
                }, false);
            }
            catch (Exception e)
            {

            }
        }
    }
}
