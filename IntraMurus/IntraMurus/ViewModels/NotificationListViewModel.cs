﻿using IntraMurus.Helpers;
using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class NotificationListViewModel : BaseViewModel
    {
        private bool _isRefreshing;
        private string _emptyListMessage;
        private Notification _notificationItem;
        private Notification _selectedNotification;

        protected UniqueExecutionTaskQueue _uniqueExecutionTaskQueue;
        protected INotificationService _notificationService;
        protected ITeamService _teamService;
        protected IPageService _pageService;

        public ObservableCollection<Notification> NotificationsDisplay { get; private set; }

        public ICommand AcceptInvitationNotificationCommand => new Command<Notification>(async n => await AcceptInvitationNotification(n));

        public NotificationListViewModel(INotificationService notificationService, ITeamService teamService, IPageService pageService, UniqueExecutionTaskQueue uniqueExecutionTaskQueue)
        {
            _notificationService = notificationService;
            _uniqueExecutionTaskQueue = uniqueExecutionTaskQueue;
            _pageService = pageService;
            _teamService = teamService;
            NotificationsDisplay = new ObservableCollection<Notification>();
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetValue(ref _isRefreshing, value); }
        }

        public string EmptyListMessage
        {
            get { return _emptyListMessage; }
            set { SetValue(ref _emptyListMessage, value); }
        }

        public Notification NotificationItem
        {
            get { return _notificationItem; }
            set { SetValue(ref _notificationItem, value); }
        }

        public Notification SelectedNotification
        {
            get => null;
            set { SetValue(ref _selectedNotification, value); }
        }

        public async Task UpdateNotificationList(Func<Task<List<Notification>>> scheduleFunc, CancellationToken token)
        {
            try
            {
                token.ThrowIfCancellationRequested();
                IsRefreshing = true;

                var notifs = await scheduleFunc();
                token.ThrowIfCancellationRequested();

                NotificationsDisplay.Clear();
                notifs.ForEach(a => NotificationsDisplay.Add(a));
                EmptyListMessage = "Aucune notification à afficher";
            }
            catch (Exception)
            {
                EmptyListMessage = "Erreur lors du téléchargement des notifications";
            }
            finally
            {
                if (!_uniqueExecutionTaskQueue.IsPending())
                {
                    IsRefreshing = false;
                }
            }
        }

        private async Task AcceptInvitationNotification(Notification notif)
        {
            if (notif.NotificationEnumType == NotificationType.Invitation)
            {
                string invitationIsApproved = "EN ATTENTE";
                CancellationToken token;
                var actionsheet = await Application.Current.MainPage.DisplayActionSheet("Voulez-vous accepter cette invitation", "Annuler", null, "Oui", "Non");
                switch (actionsheet)
                {
                    case "Annuler":
                        invitationIsApproved = "EN ATTENTE";
                        break;
                    case "Oui":
                        invitationIsApproved = "APPROUVE";
                        await _teamService.SetStatutMembreEquipe(notif, invitationIsApproved, token);
                        break;

                    case "Non":
                        invitationIsApproved = "REFUSE";
                        await _teamService.SetStatutMembreEquipe(notif, invitationIsApproved, token);
                        break;
                }
                _selectedNotification = null;
            }
           
        }
    }
}
