﻿using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using IntraMurus.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{


    public class BoardViewModel : CancellableTaskViewModel
    {
        private INotificationService _notificationService;
        private IUserService _userService;
        private INetworkService _networkService;
        private IMasterNavigationService _masterNavigationService;

        public NotificationListViewModel NotificationListViewModel { get; private set; }

        public ICommand UpdateNotificationListCommand => new Command(async () => await UpdateNotificationList());

        public ICommand CreateMessageCommand => new Command(async () => await CreateMessage());

        public ICommand OpenNewMessageWindowCommand; 

        //constructor
        public BoardViewModel(INotificationService notificationService, IUserService userService, INetworkService networkService, IMasterNavigationService nagivationService, ITeamService teamService, IPageService pageService)
        {
            _notificationService = notificationService;
            _userService = userService;
            _networkService = networkService;
            _masterNavigationService = nagivationService;
            NotificationListViewModel = new NotificationListViewModel(notificationService, teamService, pageService, _uniqueExecutionTaskQueue);
        }
       

        protected async Task UpdateNotificationList()
        {
            await _uniqueExecutionTaskQueue.Execute(async (token) =>
            {
                await NotificationListViewModel.UpdateNotificationList(async () =>
                {
                    return await _notificationService.GetNotificationsFromCIP(token);
                }
                , token);
            });
        }

        public async Task CreateMessage()
        {
            //validate if sender is captain to send
            var token = new CancellationToken();
            var teamList = new List<Team>();
            var userCip = (Application.Current as App).UserDetails.Cip;

            teamList = await _userService.GetUserTeams(token);

            for (int i = 0; i < teamList.Count; i++)
            {
               if (userCip == teamList[i].CipCapitaine)
                {
                    //open editing window
                    OpenNewMessageWindowCommand = new Command(async () => await OpenNewMessageWindow());
                }
            }

        }

        private async Task OpenNewMessageWindow()
        {
            await _masterNavigationService.NavigateToPage(new NewMessagePage());
        }
    }
}
