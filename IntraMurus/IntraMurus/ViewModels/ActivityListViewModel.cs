﻿using IntraMurus.Helpers;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using IntraMurus.ViewModels.Grouping;
using IntraMurus.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class ActivityListViewModel : BaseViewModel
    {
        private DetailedActivityViewModel _selectedActivity;
        private bool _isRefreshing;
        private string _emptyListMessage;

        protected UniqueExecutionTaskQueue _uniqueExecutionTaskQueue;
        protected IScheduleService _scheduleService;
        protected IActivityService _activityService;
        protected IUserService _userService;
        protected IMasterNavigationService _masterNavigationService;

        public DetailedActivityViewModel SelectedActivity
        {
            get { return _selectedActivity; }
            set { SetValue(ref _selectedActivity, value); }
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetValue(ref _isRefreshing, value); }
        }

        public string EmptyListMessage
        {
            get { return _emptyListMessage; }
            set { SetValue(ref _emptyListMessage, value); }
        }

        public ObservableCollection<DailyActivities> ActivitySchedule { get; private set; }

        public ICommand SelectActivityCommand => new Command<DetailedActivityViewModel>(async a => await SelectActivity(a));

        public ActivityListViewModel(IScheduleService scheduleService, IActivityService activityService, 
            IUserService userService, IMasterNavigationService masterNavigationService, UniqueExecutionTaskQueue uniqueExecutionTaskQueue)
        {
            _scheduleService = scheduleService;
            _activityService = activityService;
            _userService = userService;
            _masterNavigationService = masterNavigationService;
            _uniqueExecutionTaskQueue = uniqueExecutionTaskQueue;
            ActivitySchedule = new ObservableCollection<DailyActivities>();
        }

        public async Task UpdateActivitySchedule(Func<Task<List<DailyActivities>>> scheduleFunc, CancellationToken token)
        {
            try
            {
                token.ThrowIfCancellationRequested();
                IsRefreshing = true;

                var activities = await scheduleFunc();
                token.ThrowIfCancellationRequested();

                ActivitySchedule.Clear();
                activities.ForEach(a => ActivitySchedule.Add(a));
                EmptyListMessage = "Aucun évènement à afficher";
            }
            catch (Exception)
            {
                EmptyListMessage = "Erreur lors du téléchargement des évènements";
            }
            finally 
            {
                if (!_uniqueExecutionTaskQueue.IsPending())
                {
                    IsRefreshing = false;
                }
            }
        }

        private async Task SelectActivity(DetailedActivityViewModel activityViewModel)
        {
            if (activityViewModel == null)
                return;

            try
            {
                await _uniqueExecutionTaskQueue.Execute(async (token) =>
                {
                    var teams = await _userService.GetUserTeams(token);
                    var players = await _activityService.GetActivityPlayers(activityViewModel.Activity.ActivityId, token);
                    var userCip = (Application.Current as App).UserDetails.Cip;

                    var user = players.FirstOrDefault(p => p.CIP == userCip);
                    players.Remove(user);

                    var activity = await _scheduleService.GetDetailedActivity(activityViewModel.Activity.ActivityId, user.TeamId, token);
                    activity.Status = user.Status;
                    activityViewModel.Activity = activity;

                    activityViewModel.Activity.Players = players.OrderByDescending(p => p.Status).Where(p => p.TeamId == activityViewModel.Activity.TeamId);
                });

                await _masterNavigationService.PushAsync(new DetailedActivityPage(activityViewModel));
            }
            catch (Exception e)
            {

            }
            finally
            {
                SelectedActivity = null;
            }
        }
    }
}
