﻿using IntraMurus.Helpers;
using System;
using System.Collections.ObjectModel;

namespace IntraMurus.ViewModels.Grouping
{
    public class DailyActivities : ObservableCollection<DetailedActivityViewModel>
    {
        public DateTime Date { get; set; }

        public string DayOfWeek => DateTimeFormatHelper.GetDayOfWeek(Date).ToUpper();

        public string FormatedDate => DateTimeFormatHelper.GetFormatedDate(Date);

        public new void Add(DetailedActivityViewModel activityViewModel)
        {
            if (Date == default(DateTime))
            {
                Date = activityViewModel.Activity.Date;
            }
            else if (!Date.Date.Equals(activityViewModel.Activity.Date.Date))
            {
                throw new ArgumentException("Trying to add an activity on a different day");
            }

            base.Add(activityViewModel);
        }

        public new void Clear()
        {
            Date = default(DateTime);
            base.Clear();
        }
    }
}
