﻿using IntraMurus.Helpers;
using IntraMurus.Models;
using System;
using System.Collections.ObjectModel;

namespace IntraMurus.ViewModels.Grouping
{
    public class InscriptionStatusGroup : ObservableCollection<DetailedTeamViewModel>
    {
        public Approbation ApprobationStatus { get; set; }

        public string ApprobationStatusFormated => LanguageConverter.ApprobationTranslation(ApprobationStatus).ToUpper();

        public new void Add(DetailedTeamViewModel teamViewModel)
        {
            if (ApprobationStatus == Approbation.Invalid)
            {
                ApprobationStatus = teamViewModel.Team.Status;
            }
            else if (ApprobationStatus != teamViewModel.Team.Status)
            {
                throw new ArgumentException("Trying to add an inscription in wrong category");
            }

            base.Add(teamViewModel);
        }

        public new void Clear()
        {
            ApprobationStatus = Approbation.Invalid;
            base.Clear();
        }
    }
}
