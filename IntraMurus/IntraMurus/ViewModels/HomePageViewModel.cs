﻿using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using IntraMurus.Views;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class HomePageViewModel : CancellableTaskViewModel
    {
        private IScheduleService _scheduleService;
        private INotificationService _notificationService;
        private IMasterNavigationService _masterNavigationService;

        public ActivityListViewModel ActivityListViewModel { get; private set; }

        public NotificationListViewModel NotificationListViewModel { get; private set; }

        public ICommand ExpandBoardCommand => new Command(async () => await ExpandBoard());

        public ICommand ExpandScheduleCommand => new Command(async () => await ExpandSchedule());

        public ICommand UpdateCommand => new Command(async () => await Update());

        public HomePageViewModel(IScheduleService scheduleService, IActivityService activityService,
            IUserService userService, IMasterNavigationService masterNavigationService, INotificationService notificationService, ITeamService teamService, IPageService pageService)
        {
            ActivityListViewModel = new ActivityListViewModel(scheduleService, activityService, userService, masterNavigationService, _uniqueExecutionTaskQueue);
            NotificationListViewModel = new NotificationListViewModel(notificationService, teamService, pageService, _uniqueExecutionTaskQueue);
            _scheduleService = scheduleService;
            _masterNavigationService = masterNavigationService;
            _notificationService = notificationService;
        }

        private async Task ExpandBoard()
        {
            await _masterNavigationService.PushAsync(new BoardPage());
        }

        private async Task ExpandSchedule()
        {
            await _masterNavigationService.PushAsync(new SchedulePage());
        }

        private async Task Update()
        {
            await _uniqueExecutionTaskQueue.Execute(async (token) =>
            {
                await ActivityListViewModel.UpdateActivitySchedule(async () =>
                {
                    return await _scheduleService.GetActivitiesForNextDays(7, token);
                }
                , token);

                await NotificationListViewModel.UpdateNotificationList(async () =>
                {
                    return await _notificationService.GetNotificationsFromCIP(token);
                }
                , token);
            });
        }
    }
}
