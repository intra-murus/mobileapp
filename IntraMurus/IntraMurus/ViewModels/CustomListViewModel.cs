﻿using IntraMurus.Helpers;
using IntraMurus.ViewModels.Base;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class CustomListViewModel<TViewModel> : BaseViewModel
    {
        protected UniqueExecutionTaskQueue _uniqueExecutionTaskQueue;
        private TViewModel _selectedItem;
        private bool _isRefreshing;
        private string _emptyListMessage;

        public TViewModel SelectedItem
        {
            get { return _selectedItem; }
            set { SetValue(ref _selectedItem, value); }
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetValue(ref _isRefreshing, value); }
        }

        public string EmptyListMessage
        {
            get { return _emptyListMessage; }
            set { SetValue(ref _emptyListMessage, value); }
        }

        public ICommand SelectItemCommand => new Command<TViewModel>(async i => await SelectItem(i));

        public Func<Task> SelectItemExecutionFunc { get; set; }

        public CustomListViewModel(UniqueExecutionTaskQueue uniqueExecutionTaskQueue)
        {
            _uniqueExecutionTaskQueue = uniqueExecutionTaskQueue;
        }

        private async Task SelectItem(TViewModel viewModel)
        {
            if (viewModel == null)
                return;

            try
            {
                await SelectItemExecutionFunc();
            }
            catch (Exception)
            {

            }
            finally
            {
                SelectedItem = default(TViewModel);
            }
        }

        public async Task UpdateList(Func<Task> updateFunc, CancellationToken token)
        {
            try
            {
                token.ThrowIfCancellationRequested();
                IsRefreshing = true;

                await updateFunc();

                EmptyListMessage = "Aucun évènement à afficher";
            }
            catch (Exception)
            {
                EmptyListMessage = "Erreur lors du téléchargement des évènements";
            }
            finally
            {
                if (!_uniqueExecutionTaskQueue.IsPending())
                {
                    IsRefreshing = false;
                }
            }
        }
    }
}
