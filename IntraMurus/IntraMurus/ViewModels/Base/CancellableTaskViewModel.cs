﻿using IntraMurus.Helpers;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels.Base
{
    public class CancellableTaskViewModel : BaseViewModel
    {
        protected UniqueExecutionTaskQueue _uniqueExecutionTaskQueue;

        public ICommand CancelRunningTaskCommand => new Command(async () => await _uniqueExecutionTaskQueue.Cancel());

        public CancellableTaskViewModel()
        {
            _uniqueExecutionTaskQueue = new UniqueExecutionTaskQueue();
        }
    }
}
