﻿using Autofac;
using IntraMurus.Services;
using IntraMurus.Services.Interfaces;
using IntraMurus.Services.Mocks;
using System;
using System.Collections.Generic;
using Xamarin.Forms.Internals;

namespace IntraMurus.ViewModels.Base
{
    public static class ViewModelLocator
    {
        private static IContainer _container;
        private static List<Type> _viewModels;
        private static Dictionary<Type, Type> _serviceInstances;

        static ViewModelLocator()
        {
            _viewModels = new List<Type>
            {
                typeof(AuthentificationViewModel),
                typeof(HomePageViewModel),
                typeof(MenuPageViewModel),
                typeof(ScheduleViewModel),
                typeof(BoardViewModel),
                typeof(NewMessageViewModel),
                typeof(MyTeamsViewModel)
                //typeof(NotificationListViewModel)
            };

            _serviceInstances = new Dictionary<Type, Type>
            {
                { typeof(IPageService), typeof(PageService) },
                { typeof(IMasterNavigationService), typeof(MasterNavigationService) },
                { typeof(INetworkService), typeof(NetworkService) },
                { typeof(IActivityService), typeof(ActivityService) },
                { typeof(IScheduleService), typeof(ScheduleService) },
                { typeof(IUserService), typeof(UserService) },
                { typeof(IMemoryCacheService), typeof(MemoryCacheService) },
                { typeof(INotificationService), typeof(NotificationMocks)},
                { typeof(ITeamService), typeof(TeamService)}
            };
        }

        public static void Initialize()
        {
            if (_container != null)
                throw new InvalidOperationException("ViewModelLocator is already initialized");

            var builder = new ContainerBuilder();

            _serviceInstances.ForEach(si => builder.RegisterType(si.Value).As(si.Key).SingleInstance());
            _viewModels.ForEach(vm => builder.RegisterType(vm));

            _container = builder.Build();
        }

        public static T Resolve<T>() where T : class
        {
            return _container.Resolve<T>();
        }

        public static void RegisterService<TInterface, T>() where TInterface : class where T : class, TInterface
        {
            _serviceInstances[typeof(TInterface)] = typeof(T);
        }
    }
}
