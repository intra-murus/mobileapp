﻿using IntraMurus.Helpers;
using IntraMurus.Network.Helpers;
using IntraMurus.Network.Response;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class AuthentificationViewModel : CancellableTaskViewModel
    {
        private const string LoginUrl = "http://zeus.gel.usherbrooke.ca:8080/intramurus-api/login";

        private INetworkService _networkService;
        private IUserService _userService;
        private IMemoryCacheService _memoryCacheService;
        private IMasterNavigationService _masterNavigationService;
        private string _url;
        private bool _isLoading;

        public string Url
        {
            get { return _url; }
            set { SetValue(ref _url, value); }
        }

        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetValue(ref _isLoading, value); }
        }

        public ICommand GetUserDetailsCommand => new Command(async () => await GetUserDetails());

        public ICommand NavigatedCommand => new Command<string>(Navigated);

        public ICommand NavigatingCommand => new Command<string>(async (s) => await Navigating(s));

        public AuthentificationViewModel(INetworkService networkService, IUserService userService,
             IMemoryCacheService memoryCacheService, IMasterNavigationService masterNavigationService)
        {
            _networkService = networkService;
            _userService = userService;
            _memoryCacheService = memoryCacheService;
            _masterNavigationService = masterNavigationService;
            _uniqueExecutionTaskQueue = new UniqueExecutionTaskQueue();
            IsLoading = true;
        }

        private async Task GetUserDetails()
        {
            try
            {
                await _uniqueExecutionTaskQueue.Execute(async (token) =>
                {
                    var userDetails = await _userService.GetUserDetails(token);
                    (Application.Current as App).UserDetails = userDetails;
                    _masterNavigationService.ResetMasterPage();
                });
            }
            catch (Exception e)
            {
                Url = LoginUrl;
            }
        }

        private void Navigated(string source)
        {
            if (source.Contains(LoginUrl))
                return;

            IsLoading = false;
        }

        private async Task Navigating(string source)
        {
            IsLoading = true;

            if (!(source.Contains("jsessionid=") && !source.Contains("ticket=")))
                return;

            DependencyService.Get<IWebDataCleaner>().ClearData();

            try
            {
                await _uniqueExecutionTaskQueue.Execute(async (token) =>
                {
                    var response = await _networkService.GetAsync<AuthentificationResponse>(source.Substring(source.IndexOf("login")), token);
                    await _networkService.SetToken(response.token);
                });

                await GetUserDetails();
            }
            catch (Exception) { }
        }
    }
}
