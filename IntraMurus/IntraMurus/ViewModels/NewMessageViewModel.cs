﻿using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace IntraMurus.ViewModels
{
    public class NewMessageViewModel : CancellableTaskViewModel
    {
        private ObservableCollection<Team> UserTeams;
        private string _textInput;
        private string _teamSelected;

        //service
        private INetworkService _networkService;
        private IUserService _userService;

        public ICommand SendNewMessageCommand { get; private set; }

        public ICommand GetUserTeamsCommand; //=> new Command(async () => await GetUserTeams());

        public NewMessageViewModel(INetworkService networkService, IUserService userService)
        {
            _networkService = networkService;
            _userService = userService;
            UserTeams = new ObservableCollection<Team>();
        }

        public string TeamSelected
        {
            get { return _teamSelected; }
            set
            {
                SetValue(ref _teamSelected, value);

            }
        }

        /*
        protected async Task GetUserTeams()
        {
            await _uniqueExecutionTaskQueue.Execute(async (token) =>
            {
                return await _userService.GetUserTeams(token);

            });
        }
        */

    }
}
