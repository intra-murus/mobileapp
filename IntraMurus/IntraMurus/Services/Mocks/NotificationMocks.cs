﻿using IntraMurus.Models;
using IntraMurus.Network.Helpers;
using IntraMurus.Network.Response;
using IntraMurus.Services.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services.Mocks
{
    public class NotificationMocks : INotificationService
    {

        private readonly INetworkService _networkService;
        private readonly ITeamService _teamService;

        public NotificationMocks(INetworkService networkService, ITeamService teamService)
        {
            _networkService = networkService;
            _teamService = teamService;
        }


        public async Task<List<Notification>> GetNotificationsFromCIP(CancellationToken token)
        {
            var notificationList = new List<Notification>();
            var response = await _networkService.GetListAsync<MessageResponse>($"api/messages/getMessagesForMembre", token);
            var teamList = new List<Team>();
            
            for (int i = 0; i < response.Count; i++)
            {
                teamList.Add(await _teamService.GetTeamById(response[i].Lien, token));
                notificationList.Add(NetworkResponseConverter.ConvertMessageResponseToNotification(response[i], teamList[i].NomEquipe));
            }
            return notificationList;
        }
    }
}
