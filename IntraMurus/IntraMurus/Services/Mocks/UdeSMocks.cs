﻿using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace IntraMurus.Services.Mocks
{
    public class UdeSMocks : IUdeSService
    {
        public ObservableCollection<Student> GetAllStudentsInfo()
        {
            var str = "[{ \"app\":\"GCB202-02\",\"cip_etudiant\":\"fous2906\",\"cote_r\":35.0,\"departement\":\"1804\",\"inscription\":\"2018-09-01T04:00:34.249878Z[UTC]\",\"nom\":\"Fournel-Vézina\",\"prenom\":\"Shawn-Philippe\",\"profil_id\":\"s1\",\"programme\":\"212\",\"trimestre_id\":\"A18\",\"unit_id\":\"s1gcb20202\"},{ \"app\":\"GCB202-02\",\"cip_etudiant\":\"gara2710\",\"cote_r\":35.0,\"departement\":\"1804\",\"inscription\":\"2018-08-22T17:23:05.578901Z[UTC]\",\"nom\":\"Gareau-Lajoie\",\"prenom\":\"Antony\",\"profil_id\":\"s1\",\"programme\":\"212\",\"trimestre_id\":\"A18\",\"unit_id\":\"s1gcb20202\"},{ \"app\":\"GCB202-02\",\"cip_etudiant\":\"drea3601\",\"cote_r\":35.0,\"departement\":\"1804\",\"inscription\":\"2018-08-22T17:23:05.578901Z[UTC]\",\"nom\":\"Dresdell\",\"prenom\":\"Alice\",\"profil_id\":\"s1\",\"programme\":\"212\",\"trimestre_id\":\"A18\",\"unit_id\":\"s1gcb20202\"},{ \"app\":\"GCB202-02\",\"cip_etudiant\":\"marm3508\",\"cote_r\":35.0,\"departement\":\"1804\",\"inscription\":\"2018-08-22T17:23:05.578901Z[UTC]\",\"nom\":\"Marin\",\"prenom\":\"Mara Marlu\",\"profil_id\":\"s1\",\"programme\":\"212\",\"trimestre_id\":\"A18\",\"unit_id\":\"s1gcb20202\"}]";
            ObservableCollection<Student> students = JsonConvert.DeserializeObject<ObservableCollection<Student>>(str);
            
            return students;
        }
    }
}
