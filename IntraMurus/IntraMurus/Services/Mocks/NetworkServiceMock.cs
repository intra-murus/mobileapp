﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IntraMurus.Helpers;
using IntraMurus.Network.Response;
using IntraMurus.Services.Interfaces;
using Newtonsoft.Json;

namespace IntraMurus.Services.Mocks
{
    public class NetworkServiceMock : INetworkService
    {
        private List<ScheduleResponse> _lastScheduleResponses = new List<ScheduleResponse>();

        public Task<TResp> GetAsync<TResp>(string path, CancellationToken cancellationToken) where TResp : BaseResponse
        {
            throw new NotImplementedException();
        }

        public async Task<List<TResp>> GetListAsync<TResp>(string path, CancellationToken cancellationToken)
        {
            await Task.Delay(100, cancellationToken);

            return GenerateResponses<TResp>(path);
        }

        public Task<TResp> PostAsync<TReq, TResp>(string path, TReq requestObject, CancellationToken cancellationToken) where TResp : BaseResponse
        {
            throw new NotImplementedException();
        }

        public Task<TResp> PutAsync<TReq, TResp>(string path, TReq requestObject, CancellationToken cancellationToken) where TResp : BaseResponse
        {
            throw new NotImplementedException();
        }

        public Task SetToken(string token) { return Task.FromResult(0); }

        private List<TResp> GenerateResponses<TResp>(string path)
        {
            if (typeof(TResp) == typeof(ScheduleResponse))
            {
                var scheduleResponses = GenerateScheduleResponses();
                _lastScheduleResponses = (List<ScheduleResponse>)scheduleResponses;
                return (List<TResp>) scheduleResponses;
            }
            else if (typeof(TResp) == typeof(DetailedActivityResponse))
            {
                if (int.TryParse(path.Substring(path.LastIndexOf('=') + 1, path.Length - path.LastIndexOf('=') - 1), out var value))
                {
                    var scheduleResponse = _lastScheduleResponses.Single(r => r.idMatch == value);

                    var json = JsonConvert.SerializeObject(scheduleResponse);
                    var detailedActivityResponse = JsonConvert.DeserializeObject<DetailedActivityResponse>(json) ;

                    return (new List<TResp>() { (TResp) (detailedActivityResponse as object) });
                }
            }

            return new List<TResp>();
        }

        private object GenerateScheduleResponses()
        {
            var utcDate = DateTime.Now.Date - TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

            return new List<ScheduleResponse>
            {
                new ScheduleResponse
                {
                    idMatch = 1,
                    terrain = "ANNEXE",
                    statutMatch = "EN_ATTENTE",
                    dateDebut = (utcDate + new TimeSpan(2, 20, 0, 0)).ToUnixTimeMilliseconds(),
                    dateFin = (utcDate + new TimeSpan(2, 21, 45, 0)).ToUnixTimeMilliseconds(),
                    statutMembreMatch = "PRESENT",
                    sport = "SOCCER_INTERIEUR",
                    nomLigue = "AA",
                    nomEquipe = "Bitconnect"
                },
                new ScheduleResponse
                {
                    idMatch = 2,
                    terrain = "ANNEXE",
                    statutMatch = "EN_ATTENTE",
                    dateDebut = (utcDate + new TimeSpan(2, 20, 0, 0)).ToUnixTimeMilliseconds(),
                    dateFin = (utcDate + new TimeSpan(2, 21, 0, 0)).ToUnixTimeMilliseconds(),
                    statutMembreMatch = "ABSENT",
                    sport = "SOCCER_INTERIEUR",
                    nomLigue = "AA",
                    nomEquipe = "Bitconnect"
                },
                new ScheduleResponse
                {
                    idMatch = 3,
                    terrain = "ANNEXE",
                    statutMatch = "EN_ATTENTE",
                    dateDebut = (utcDate + new TimeSpan(20, 0, 0)).ToUnixTimeMilliseconds(),
                    dateFin = (utcDate + new TimeSpan(21, 0, 0)).ToUnixTimeMilliseconds(),
                    statutMembreMatch = "EN_ATTENTE",
                    sport = "SOCCER_INTERIEUR",
                    nomLigue = "AA",
                    nomEquipe = "Bitconnect"
                },
                new ScheduleResponse
                {
                    idMatch = 4,
                    terrain = "ANNEXE",
                    statutMatch = "EN_ATTENTE",
                    dateDebut = (utcDate + new TimeSpan(3, 22, 0, 0)).ToUnixTimeMilliseconds(),
                    dateFin = (utcDate + new TimeSpan(3, 22, 45, 0)).ToUnixTimeMilliseconds(),
                    statutMembreMatch = "EN_ATTENTE",
                    sport = "SOCCER_INTERIEUR",
                    nomLigue = "AA",
                    nomEquipe = "Bitconnect"
                },
            }; 
        }
    }
}
