﻿using System;
using System.Collections.Generic;
using System.Text;
using IntraMurus.Services.Interfaces;

namespace IntraMurus.Services.Mocks
{
    public class SportMocks : ISportsService
    {
        public List<String> GetAllSports()
        {
            var sports = new List<String>
            {
                "Soccer",
                "VolleyBall",
                "Basketball",
                "Hockey Cosom",
                "Frisbee Ultime"
            };

            return sports;
        }
    }
}
