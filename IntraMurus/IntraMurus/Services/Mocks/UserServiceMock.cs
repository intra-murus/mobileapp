﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using IntraMurus.Models;
using IntraMurus.Services.Interfaces;

namespace IntraMurus.Services.Mocks
{
    public class UserServiceMock : IUserService
    {
        public Task<UserDetails> GetUserDetails(CancellationToken token)
        {
            return Task.FromResult(new UserDetails
            {
                Cip = "test1234",
            });
        }

        public Task<List<Team>> GetUserTeams(CancellationToken token)
        {
            return Task.FromResult(new List<Team>
            {
                new Team
                {
                    InscriptionData = new Inscription
                    {
                        Category = "A",
                        SportName = "Soccer",
                        TeamName = "BitConnect",
                        PlayersList = new List<Player>
                        {
                            new Player
                            {
                                Name = "Big Pat",
                                CIP = "test1234",
                                ConfirmationStatus = Confirmation.Confirmed
                            }
                        },
                        Year = 2018,
                        Period = Period.Winter
                    },
                    Status = Approbation.Approved
                },
                new Team
                {
                    InscriptionData = new Inscription
                    {
                        Category = "AA",
                        SportName = "Basketball",
                        TeamName = "Les Jordans",
                        PlayersList = new List<Player>
                        {
                            new Player
                            {
                                Name = "Big Pat",
                                CIP = "test1234",
                                ConfirmationStatus = Confirmation.Confirmed
                            }
                        },
                        Year = 2018,
                        Period = Period.Winter
                    },
                    Status = Approbation.Waiting
                },
                new Team
                {
                    InscriptionData = new Inscription
                    {
                        Category = "B",
                        SportName = "Volleyball",
                        TeamName = "Les PAN",
                        PlayersList = new List<Player>
                        {
                            new Player
                            {
                                Name = "Big Pat",
                                CIP = "test1234",
                                ConfirmationStatus = Confirmation.Cancelled
                            }
                        },
                        Year = 2018,
                        Period = Period.Winter
                    },
                    Status = Approbation.Unspecified
                }
            });
        }
    }
}
