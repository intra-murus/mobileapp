﻿using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services.Mocks
{
    public class ActivityServiceMock : IActivityService
    {
        public Task<List<Player>> GetActivityPlayers(int match, CancellationToken token)
        {
            return Task.FromResult(new List<Player>
            {
                new Player
                {
                    Name = "Big Pat",
                    CIP = "test1234",
                    TeamId = 1,
                    Status = Availability.Present
                }
            });
        }

        public Task UpdatePlayerStatus(Activity activity, CancellationToken token)
        {
            return Task.FromResult(0);
        }

        public Task UpdateTeamStatus(Activity activity, CancellationToken token)
        {
            return Task.FromResult(0);
        }
    }
}
