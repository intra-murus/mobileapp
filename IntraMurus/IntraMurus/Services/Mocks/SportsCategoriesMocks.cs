﻿using IntraMurus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace IntraMurus.Services.Mocks
{
    public class SportsCategoriesMocks : ISportsCategoriesService
    {
        public List<string> GetAllCategories()
        {
            var myList = new List<string>();
            myList.Add("AA");
            myList.Add("A");
            myList.Add("B");
            myList.Add("C");
            myList.Add("D");
            return myList;
        }

        public ObservableCollection<string> GetSportCategories(string sportname)
        {
            var myList = new ObservableCollection<string>();
            switch (sportname)
            {
                case "Soccer":
                    myList.Add("AA");
                    myList.Add("AA");
                    myList.Add("AA");
                    myList.Add("AA");
                    break;
                case "Volleyball":
                    myList.Add("A");
                    myList.Add("B");
                    myList.Add("C");
                    break;


            }
            return myList;
        }
    }
}
