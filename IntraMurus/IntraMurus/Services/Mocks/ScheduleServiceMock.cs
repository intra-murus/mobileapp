﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Grouping;
using Xamarin.Forms.Internals;

namespace IntraMurus.Services.Mocks
{
    public class ScheduleServiceMock : IScheduleService
    {
        private List<DailyActivities> _lastCreatedActivities;
        private IActivityService _activityService;
        private IPageService _pageService;

        public ScheduleServiceMock(IActivityService activityService, IPageService pageService)
        {
            _activityService = activityService;
            _pageService = pageService;
            _lastCreatedActivities = new List<DailyActivities>();
        }

        public Task<Activity> GetDetailedActivity(int activityId, int teamId, CancellationToken token)
        {
            var activities = new List<Activity>();
            _lastCreatedActivities.ForEach(da => da.ForEach(a => activities.Add(a.Activity)));
            return Task.FromResult(activities.FirstOrDefault(a => a.ActivityId == activityId));
        }

        public Task<List<DailyActivities>> GetActivitiesByMonthOfYear(int month, int year, CancellationToken token)
        {
            _lastCreatedActivities = new List<DailyActivities>
            {
                GenerateDailyActivities(new DateTime(year, month, 11)),
                GenerateDailyActivities(new DateTime(year, month, 12))
            };

            return Task.FromResult(_lastCreatedActivities);
        }

        public List<DailyActivities> GetActivitiesByWeek(DateTime date)
        {
            var firstDayOfWeek = date.AddDays(-(int)date.DayOfWeek);

            _lastCreatedActivities = new List<DailyActivities>
            {
                GenerateDailyActivities(firstDayOfWeek),
                GenerateDailyActivities(firstDayOfWeek.AddDays(2)),
                GenerateDailyActivities(firstDayOfWeek.AddDays(5))
            };

            return _lastCreatedActivities;
        }

        public Task<List<DailyActivities>> GetActivitiesForNextDays(int days, CancellationToken token)
        {
            _lastCreatedActivities = new List<DailyActivities>
            {
                GenerateDailyActivities(DateTime.Now.Date),
                GenerateDailyActivities(DateTime.Now.Date.AddDays(new Random().Next(1, days)))
            };
            return Task.FromResult(_lastCreatedActivities);
        }

        private DailyActivities GenerateDailyActivities(DateTime date)
        {
            return new DailyActivities
            {
                new DetailedActivityViewModel(new Activity
                {
                    ActivityId = 1,
                    Name = "Soccer A",
                    Date = date.AddHours(9).AddMinutes(30),
                    Duration = new TimeSpan(1, 30, 0),
                    Location = "Terrain 2",
                    TeamName = "Bitconnect",
                    Status = Availability.Absent,
                    Players = _players
                }, _activityService, _pageService),

                new DetailedActivityViewModel(new Activity
                {
                    ActivityId = 2,
                    Name = "Soccer B",
                    Date = date.AddHours(10).AddMinutes(30),
                    Duration = new TimeSpan(1, 30, 0),
                    Location = "Terrain 2",
                    TeamName = "Bitconnect",
                    Status = Availability.None,
                    Players = _players
                }, _activityService, _pageService),

                new DetailedActivityViewModel(new Activity
                {
                    ActivityId = 3,
                    Name = "Soccer C",
                    Date = date.AddHours(12).AddMinutes(30),
                    Duration = new TimeSpan(1, 0, 0),
                    Location = "Terrain 3",
                    TeamName = "Bitconnect",
                    Status = Availability.Unspecified,
                    ConfirmationLimit = date.AddHours(1),
                    Players = _players
                }, _activityService, _pageService),
            };
        }

        private ICollection<Player> _players => new List<Player>
        {
            new Player { Name = "Gibier de potence", Status = Availability.Present },
            new Player { Name = "Sinistre Individu", Status = Availability.Present },
            new Player { Name = "Vieux Pourri", Status = Availability.Absent },
            new Player { Name = "Mitel de galapiat", Status = Availability.Unspecified }
        };
    }
}
