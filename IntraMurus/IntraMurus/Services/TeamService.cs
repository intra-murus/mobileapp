﻿using IntraMurus.Models;
using IntraMurus.Network;
using IntraMurus.Network.Helpers;
using IntraMurus.Network.Request;
using IntraMurus.Network.Response;
using IntraMurus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace IntraMurus.Services
{
    public class TeamService : ITeamService
    {
        private readonly INetworkService _networkService;

        public TeamService(INetworkService networkService)
        {
            _networkService = networkService;
        }
        public async Task<Team> GetTeamById(int Id, CancellationToken token)
        {
            var response = await _networkService.GetAsync<UserTeamResponse>($"api/equipes/getEquipeById?id_equipe={Id}", token);
            return NetworkResponseConverter.ConvertUserTeamResponseToUserTeam(response);
        }

        public async Task<List<Player>> GetTeamMembers(int Id, CancellationToken token)
        {
            var response = await _networkService.GetListAsync<PlayerResponse>($"api/equipes/getMembresInEquipe?idEquipe={Id}", token);
            return response.Select(r => NetworkResponseConverter.ConvertPlayerResponseToPlayer(r)).ToList();
        }

        public async Task PutStatutApprobation(int equipe, CancellationToken token)
        {
            var request = new PutStatusApprobationRequest { idEquipe = equipe, statutApprobation = "PRET_A_APPROUVER" };
            var response = await _networkService.PutAsync<PutStatusApprobationRequest, BaseResponse>($"api/equipes/updateStatutApprobation", request, token);
        }

        public async Task<ApiStatus> SetStatutMembreEquipe(Notification notification, string invitationIsApproved, CancellationToken token)
        {
            try
            {
                PutStatusTeamMember status = new PutStatusTeamMember();
                status.cip = (Application.Current as App).UserDetails.Cip;
                status.idEquipe = notification.IdSender;

                if (invitationIsApproved == "APPROUVE")
                {
                    status.statutMembreEquipe = "ACCEPTE";
                }
                else if (invitationIsApproved == "REFUSE")
                {
                    status.statutMembreEquipe = "REFUSE";
                }
                else
                {
                    status.statutMembreEquipe = "EN_ATTENTE";
                }
                var response = await _networkService.PutAsync<PutStatusTeamMember, BaseResponse>($"api/equipes/updateStatutMembreEquipe", status, token);
                return ApiStatus.OK;
            }
            catch(Exception e)
            {
                return ApiStatus.BadRequest;
            }
         
        }

    }
}
