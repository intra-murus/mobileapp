﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace IntraMurus.Services.Interfaces
{
    public interface IMasterNavigationService
    {
        Task PushAsync<T>(T page) where T : Page;

        Task NavigateToPage<T>(T page) where T : Page;

        void ResetMasterPage();

        void SetMainPage<T>(T page) where T : Page;
    }
}
