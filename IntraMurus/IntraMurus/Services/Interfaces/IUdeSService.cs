﻿using IntraMurus.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace IntraMurus.Services.Interfaces
{
    public interface IUdeSService
    {
        ObservableCollection<Student> GetAllStudentsInfo();
    }
}
