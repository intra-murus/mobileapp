﻿using IntraMurus.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services.Interfaces
{
    public interface IActivityService
    {
        Task<List<Player>> GetActivityPlayers(int match, CancellationToken token);

        Task UpdatePlayerStatus(Activity activity, CancellationToken token);

        Task UpdateTeamStatus(Activity activity, CancellationToken token);
    }
}
