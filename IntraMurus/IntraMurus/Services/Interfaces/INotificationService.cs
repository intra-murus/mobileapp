﻿using IntraMurus.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services.Interfaces
{
    public interface INotificationService
    {
      Task<List<Notification>> GetNotificationsFromCIP(CancellationToken token);
    }
}
