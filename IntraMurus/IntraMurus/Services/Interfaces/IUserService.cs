﻿using IntraMurus.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserDetails> GetUserDetails(CancellationToken token);

        Task<List<Team>> GetUserTeams(CancellationToken token);
    }
}
