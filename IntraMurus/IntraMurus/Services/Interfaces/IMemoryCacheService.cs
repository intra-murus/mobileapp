﻿using System;

namespace IntraMurus.Services.Interfaces
{
    public interface IMemoryCacheService
    {
        void Set<T>(string key, T value);

        void Set<T>(string key, T value, DateTimeOffset absoluteExpiry);

        T Get<T>(string key);
    }
}
