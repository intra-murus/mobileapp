﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntraMurus.Services.Interfaces
{
    public interface ISportsService
    {
        List<String> GetAllSports();
    }
}
