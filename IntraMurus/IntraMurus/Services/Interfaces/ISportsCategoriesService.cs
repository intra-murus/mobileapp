﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace IntraMurus.Services.Interfaces
{
    public interface ISportsCategoriesService
    {
        List<string> GetAllCategories();

        ObservableCollection<string> GetSportCategories(string sportname);
    }
}
