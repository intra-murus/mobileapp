﻿using IntraMurus.Models;
using IntraMurus.Network;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services.Interfaces
{
    public interface ITeamService
    {
        Task<Team> GetTeamById(int Id, CancellationToken token);

        Task<List<Player>> GetTeamMembers(int Id, CancellationToken token);

        Task PutStatutApprobation(int equipe, CancellationToken token);

        Task<ApiStatus> SetStatutMembreEquipe(Notification notification, string invitationIsApproved, CancellationToken token);
    }
}
