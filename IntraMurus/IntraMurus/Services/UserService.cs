﻿using IntraMurus.Models;
using IntraMurus.Network.Helpers;
using IntraMurus.Network.Response;
using IntraMurus.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services
{
    public class UserService : IUserService
    {
        private readonly INetworkService _networkService;

        public UserService(INetworkService networkService)
        {
            _networkService = networkService;
        }

        public async Task<UserDetails> GetUserDetails(CancellationToken token)
        {
            var response = await _networkService.GetAsync<UserDetailsResponse>($"api/user", token);
            return NetworkResponseConverter.ConvertUserDetailsResponseToUserDetails(response);
        }

        public async Task<List<Team>> GetUserTeams(CancellationToken token)
        {
            var response = await _networkService.GetListAsync<UserTeamResponse>($"api/equipes/getEquipesForMembre", token);
            return response.Select(r => NetworkResponseConverter.ConvertUserTeamResponseToUserTeam(r)).ToList();
        }
    }
}
