﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IntraMurus.Helpers;
using IntraMurus.Models;
using IntraMurus.Network.Helpers;
using IntraMurus.Network.Response;
using IntraMurus.Services.Interfaces;
using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Grouping;
using Xamarin.Forms.Internals;

namespace IntraMurus.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly INetworkService _networkService;
        private readonly IActivityService _activityService;
        private readonly IPageService _pageService;

        public ScheduleService(INetworkService networkService, IActivityService activityService, IPageService pageService)
        {
            _networkService = networkService;
            _activityService = activityService;
            _pageService = pageService;
        }

        public async Task<Activity> GetDetailedActivity(int activityId, int teamId, CancellationToken token)
        {
            var response = await _networkService.GetListAsync<DetailedActivityResponse>($"api/matchs/getEquipesForMatch?matchId={activityId}", token);
            return NetworkResponseConverter.ConvertDetailedActivityResponseToActivity(response.FirstOrDefault(r => r.idEquipe == teamId));
        }

        public async Task<List<DailyActivities>> GetActivitiesByMonthOfYear(int month, int year, CancellationToken token)
        {
            var startDate = new DateTime(year, month, 1).ToUnixTimeSeconds();
            var endDate = new DateTime(year, month, 1).AddMonths(1).ToUnixTimeSeconds();

            return await GetActivities($"api/horaire?startDate={startDate}&endDate={endDate}", token);
        }

        public List<DailyActivities> GetActivitiesByWeek(DateTime date)
        {
            throw new NotImplementedException();
        }

        public async Task<List<DailyActivities>> GetActivitiesForNextDays(int days, CancellationToken token)
        {
            var startDate = DateTime.UtcNow.Date.ToUnixTimeSeconds();
            var endDate = DateTime.UtcNow.Date.AddDays(days).ToUnixTimeSeconds();

            return await GetActivities($"api/horaire?startDate={startDate}&endDate={endDate}", token);
        }

        private async Task<List<DailyActivities>> GetActivities(string path, CancellationToken token)
        {
            var dailyActivitiesList = new List<DailyActivities>();

            var response = await _networkService.GetListAsync<ScheduleResponse>(path, token);
            var activities = response.Select(r => NetworkResponseConverter.ConvertScheduleResponseToActivity(r)).ToList();

            activities.Sort();
            var lookupTable = activities.ToLookup(p => p.Date.Date, p => p);

            foreach (var groupItem in lookupTable)
            {
                var dailyActivities = new DailyActivities();
                groupItem.ForEach(item => dailyActivities.Add(new DetailedActivityViewModel(item, _activityService, _pageService)));
                dailyActivitiesList.Add(dailyActivities);
            }

            return dailyActivitiesList;
        }
    }
}
