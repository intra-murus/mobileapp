﻿using IntraMurus.Models;
using IntraMurus.Network.Helpers;
using IntraMurus.Network.Request;
using IntraMurus.Network.Response;
using IntraMurus.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Services
{
    public class ActivityService : IActivityService
    {
        private readonly INetworkService _networkService;

        public ActivityService(INetworkService networkService)
        {
            _networkService = networkService;
        }

        public async Task<List<Player>> GetActivityPlayers(int match, CancellationToken token)
        {
            var response = await _networkService.GetListAsync<PlayerResponse>($"api/matchs/getMembresForMatch?matchId={match}", token);
            return response.Select(r => NetworkResponseConverter.ConvertPlayerResponseToPlayer(r)).ToList();
        }

        public async Task UpdatePlayerStatus(Activity activity, CancellationToken token)
        {
            var request = NetworkRequestConverter.ConvertActivityToPutPlayerStatusRequest(activity);
            var response = await _networkService.PutAsync<PutPlayerStatusRequest, BaseResponse>("api/matchs/updateStatutMembreMatch", request, token);
        }

        public async Task UpdateTeamStatus(Activity activity, CancellationToken token)
        {
            var request = NetworkRequestConverter.ConvertActivityToPutTeamStatusRequest(activity);
            var response = await _networkService.PutAsync<PutTeamStatusRequest, BaseResponse>("api/matchs/updateStatutEquipeMatch", request, token);
        }
    }
}
