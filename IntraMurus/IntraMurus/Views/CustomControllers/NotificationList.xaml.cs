﻿using IntraMurus.Helpers;
using IntraMurus.Models;
using IntraMurus.Services;
using IntraMurus.Services.Interfaces;
using IntraMurus.Services.Mocks;
using IntraMurus.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views.CustomControllers
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NotificationList : ContentView
	{
        public static readonly BindableProperty IsRefreshingProperty =
            BindableProperty.Create(nameof(IsRefreshing), typeof(bool), typeof(NotificationList), false, BindingMode.TwoWay);

        public static readonly BindableProperty EmptyListMessageProperty =
           BindableProperty.Create(nameof(EmptyListMessage), typeof(string), typeof(NotificationList));

        public static readonly BindableProperty NotificationsProperty =
            BindableProperty.Create(nameof(Notifications), typeof(ICollection<Notification>), typeof(NotificationList));

        public static readonly BindableProperty AcceptInvitationNotificationCommandProperty =
          BindableProperty.Create(nameof(AcceptInvitationNotificationCommand), typeof(ICommand), typeof(NotificationList));

        public bool IsRefreshing
        {
            get { return (bool)GetValue(IsRefreshingProperty); }
            set { SetValue(IsRefreshingProperty, value); }
        }

        public string EmptyListMessage
        {
            get { return (string)GetValue(EmptyListMessageProperty); }
            set { SetValue(EmptyListMessageProperty, value); }
        }

        public ICollection<Notification> Notifications
        {
            get { return (ICollection<Notification>)GetValue(NotificationsProperty); }
            set { SetValue(NotificationsProperty, value); }
        }

        public NotificationList ()
		{
			InitializeComponent ();

           
        }

        public ICommand AcceptInvitationNotificationCommand
        {
            get { return (ICommand)GetValue(AcceptInvitationNotificationCommandProperty); }
            set { SetValue(AcceptInvitationNotificationCommandProperty, value); }
        }

        protected void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (AcceptInvitationNotificationCommand?.CanExecute(e.SelectedItem) ?? false)
            {
                AcceptInvitationNotificationCommand?.Execute(e.SelectedItem);            
            }
            
        }
    }
}