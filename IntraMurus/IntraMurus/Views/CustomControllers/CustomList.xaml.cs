﻿using System;
using System.Collections;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views.CustomControllers
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomList : ContentView
	{
        public static readonly BindableProperty SelectedItemProperty =
            BindableProperty.Create(nameof(SelectedItem), typeof(object), typeof(CustomList), null, BindingMode.TwoWay);

        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(nameof(ItemsSource), typeof(IEnumerable), typeof(CustomList), null);

        public static readonly BindableProperty SelectItemCommandProperty =
            BindableProperty.Create(nameof(SelectItemCommand), typeof(ICommand), typeof(CustomList));

        public static readonly BindableProperty GroupingCellProperty =
            BindableProperty.Create(nameof(GroupingCell), typeof(ViewCell), typeof(CustomList));

        public static readonly BindableProperty ItemCellProperty =
            BindableProperty.Create(nameof(ItemCell), typeof(ViewCell), typeof(CustomList));

        public static readonly BindableProperty IsRefreshingProperty =
            BindableProperty.Create(nameof(IsRefreshing), typeof(bool), typeof(CustomList), false, BindingMode.TwoWay);

        public static readonly BindableProperty EmptyListMessageProperty =
            BindableProperty.Create(nameof(EmptyListMessage), typeof(string), typeof(CustomList));

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public ICommand SelectItemCommand
        {
            get { return (ICommand)GetValue(SelectItemCommandProperty); }
            set { SetValue(SelectItemCommandProperty, value); }
        }

        public ViewCell GroupingCell
        {
            get { return (ViewCell)GetValue(GroupingCellProperty); }
            set { SetValue(GroupingCellProperty, value); }
        }

        public ViewCell ItemCell
        {
            get { return (ViewCell)GetValue(ItemCellProperty); }
            set { SetValue(ItemCellProperty, value); }
        }

        public bool IsRefreshing
        {
            get { return (bool)GetValue(IsRefreshingProperty); }
            set { SetValue(IsRefreshingProperty, value); }
        }

        public string EmptyListMessage
        {
            get { return (string)GetValue(EmptyListMessageProperty); }
            set { SetValue(EmptyListMessageProperty, value); }
        }

        public CustomList()
        {
            InitializeComponent();
            list.GroupHeaderTemplate = new DataTemplate(() => Activator.CreateInstance(GroupingCell.GetType()));
            list.ItemTemplate = new DataTemplate(() => Activator.CreateInstance(ItemCell.GetType()));
        }

        protected void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (SelectItemCommand?.CanExecute(e.SelectedItem) ?? false)
            {
                SelectItemCommand?.Execute(e.SelectedItem);
            }
        }
    }
}