﻿using IntraMurus.Models;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views.CustomControllers
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PresenceSelection : ContentView
	{
        public static readonly BindableProperty AvailabilityProperty =
            BindableProperty.Create(nameof(Availability), typeof(Availability), typeof(PresenceSelection), Availability.Unspecified, BindingMode.TwoWay);

        public static readonly BindableProperty SelectionCommandProperty =
            BindableProperty.Create(nameof(SelectionCommand), typeof(ICommand), typeof(PresenceSelection));

        public Availability Availability
        {
            get { return (Availability)GetValue(AvailabilityProperty); }
            set { SetValue(AvailabilityProperty, value); }
        }

        public ICommand SelectionCommand
        {
            get { return (ICommand)GetValue(SelectionCommandProperty); }
            set { SetValue(SelectionCommandProperty, value); }
        }

        public ICommand ButtonPresentClickedCommand => new Command<Availability>(UpdateAvailability);

        public ICommand ButtonAbsentClickedCommand => new Command<Availability>(UpdateAvailability);

        public ICommand ButtonUnspecifiedClickedCommand => new Command<Availability>(UpdateAvailability);

        public PresenceSelection()
		{
			InitializeComponent();
		}

        private void UpdateAvailability(Availability availability)
        {
            if (SelectionCommand?.CanExecute(availability) ?? false)
            {
                SelectionCommand?.Execute(availability);
            }
        }

    }
}