﻿using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.Services.Mocks;
using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BoardPage : ContentPage
	{
        private BoardViewModel _viewModel;
        public BoardPage()
		{
			InitializeComponent();

            _viewModel = ViewModelLocator.Resolve<BoardViewModel>();
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            _viewModel?.UpdateNotificationListCommand?.Execute(null);
        }

        protected override void OnDisappearing()
        {
            _viewModel?.CancelRunningTaskCommand?.Execute(null);
        }

        private void ToolbarItem_Clicked(object sender, System.EventArgs e)
        {

            _viewModel?.CreateMessageCommand?.Execute(null);
        }
    }
}