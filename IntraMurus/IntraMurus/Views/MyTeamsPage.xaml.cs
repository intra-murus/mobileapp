﻿using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyTeamsPage : ContentPage
    {
        private MyTeamsViewModel _viewModel;

        public MyTeamsPage()
        {
            InitializeComponent();

            _viewModel = ViewModelLocator.Resolve<MyTeamsViewModel>();
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            _viewModel?.UpdateTeamsCommand?.Execute(null);
        }

        protected override void OnDisappearing()
        {
            _viewModel?.CancelRunningTaskCommand?.Execute(null);
        }
    }
}
