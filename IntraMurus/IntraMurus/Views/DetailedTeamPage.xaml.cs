﻿using IntraMurus.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailedTeamPage : ContentPage
	{
        private DetailedTeamViewModel _viewModel;

        public DetailedTeamPage(DetailedTeamViewModel viewModel)
		{
			InitializeComponent();

            _viewModel = viewModel;
            BindingContext = _viewModel;
		}

        protected override void OnDisappearing()
        {
            _viewModel?.CancelRunningTaskCommand?.Execute(null);
        }
    }
}