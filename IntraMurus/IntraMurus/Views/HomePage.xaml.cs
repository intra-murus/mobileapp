﻿using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
	{
        private HomePageViewModel _viewModel;

        public HomePage()
		{
			InitializeComponent();

            _viewModel = ViewModelLocator.Resolve<HomePageViewModel>();
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            _viewModel?.UpdateCommand?.Execute(null);
        }

        protected override void OnDisappearing()
        {
            _viewModel?.CancelRunningTaskCommand?.Execute(null);
        }
    }
}
