﻿using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AuthentificationPage : ContentPage
	{
        private AuthentificationViewModel _viewModel;

        public AuthentificationPage()
		{
			InitializeComponent();

            _viewModel = ViewModelLocator.Resolve<AuthentificationViewModel>();
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            _viewModel?.GetUserDetailsCommand?.Execute(null);
        }

        protected override void OnDisappearing()
        {
            _viewModel?.CancelRunningTaskCommand?.Execute(null);
        }

        private void OnNavigating(object sender, WebNavigatingEventArgs e)
        {
            _viewModel?.NavigatingCommand?.Execute(e.Url);
        }

        private void OnNavigated(object sender, WebNavigatedEventArgs e)
        {
            _viewModel?.NavigatedCommand?.Execute(e.Url);
        }
    }
}