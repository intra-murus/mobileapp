﻿using IntraMurus.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailedActivityPage : ContentPage
	{
        private DetailedActivityViewModel _viewModel;

        public DetailedActivityPage(DetailedActivityViewModel viewModel)
		{
			InitializeComponent();

            _viewModel = viewModel;
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            _viewModel?.OnAppearingCommand?.Execute(null);
        }

        protected override void OnDisappearing()
        {
            _viewModel?.CancelRunningTaskCommand?.Execute(null);
            _viewModel?.OnDisappearingCommand?.Execute(null);
        }
    }
}