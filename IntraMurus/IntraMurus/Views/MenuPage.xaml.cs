﻿using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : ContentPage
	{
        public MenuPage()
		{
			InitializeComponent();
            
            BindingContext = ViewModelLocator.Resolve<MenuPageViewModel>();
		}
	}
}