﻿using IntraMurus.ViewModels;
using IntraMurus.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewMessagePage : ContentPage
    {

        private NewMessageViewModel _viewModel;


        public NewMessagePage()
        {
            InitializeComponent();

            _viewModel = ViewModelLocator.Resolve<NewMessageViewModel>();
            BindingContext = _viewModel;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            _viewModel?.SendNewMessageCommand?.Execute(null);
        }

        protected override void OnAppearing()
        {
            _viewModel?.GetUserTeamsCommand?.Execute(null);

        }

        protected override void OnDisappearing()
        {
            _viewModel?.CancelRunningTaskCommand?.Execute(null);
        }
    }
}