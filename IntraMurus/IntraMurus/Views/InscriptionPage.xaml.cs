﻿using IntraMurus.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using IntraMurus.Services;
using IntraMurus.Services.Mocks;
using IntraMurus.Models;
using System;

namespace IntraMurus.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InscriptionPage : ContentPage
    {
        public InscriptionPage()
        {
            InitializeComponent();
        
            BindingContext = new InscriptionViewModel(new PageService(), new SportMocks(), new SportsCategoriesMocks(), new MemoryCacheService(), new NetworkService(new MasterNavigationService()), new UdeSMocks());
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {       
            if (e.NewTextValue is string searchStr)
            {
                (BindingContext as InscriptionViewModel).CIPListSearchCommand.Execute(searchStr);
            }
            
        }

        private void DeleteListPlayerButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var player = button.BindingContext as Player;

            (BindingContext as InscriptionViewModel).RemovePlayerFromListCommand.Execute(player);
        }

        protected override void OnAppearing()
        {
            (BindingContext as InscriptionViewModel)?.OnAppearingCommandList?.Execute(null);
            (BindingContext as InscriptionViewModel)?.OnAppearingCommandCache?.Execute(null);

        }

        protected override void OnDisappearing()
        {
            (BindingContext as InscriptionViewModel)?.OnDisappearingCommand?.Execute(null);
        }

        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            (BindingContext as InscriptionViewModel)?.SportPickerCommand?.Execute(null);
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            (BindingContext as InscriptionViewModel)?.EntryChangedCommand?.Execute(e.NewTextValue);
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            (BindingContext as InscriptionViewModel)?.SelectedStudentFromListCommand?.Execute(null);
        }
    }
}