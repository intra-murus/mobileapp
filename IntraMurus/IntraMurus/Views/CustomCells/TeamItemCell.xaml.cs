﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views.CustomCells
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TeamItemCell : ViewCell
	{
		public TeamItemCell()
		{
			InitializeComponent();
		}
	}
}