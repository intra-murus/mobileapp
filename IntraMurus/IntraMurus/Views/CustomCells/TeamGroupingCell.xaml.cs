﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views.CustomCells
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TeamGroupingCell : ViewCell
	{
		public TeamGroupingCell()
		{
			InitializeComponent();
		}
	}
}