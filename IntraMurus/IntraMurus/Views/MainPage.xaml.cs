﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IntraMurus.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : MasterDetailPage
    {
		public MainPage()
		{
			InitializeComponent();
		}
	}
}