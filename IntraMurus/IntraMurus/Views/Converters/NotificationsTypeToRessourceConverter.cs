﻿using IntraMurus.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace IntraMurus.Views.Converters
{
    public class NotificationsTypeToRessourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is NotificationType notificationType)
            {
                return ImageSource.FromResource($"IntraMurus.Resources.{notificationType.ToString().ToLower()}.png");
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
