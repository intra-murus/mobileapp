﻿using IntraMurus.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace IntraMurus.Views.Converters
{
    public class StatusToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is Approbation approbation && (approbation == Approbation.Unspecified || approbation == Approbation.Waiting);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
