﻿namespace IntraMurus.Views.Converters
{
    public class StatusToBackgroundColorConverter : StatusToColorConverter
    {
        public StatusToBackgroundColorConverter()
        {
            _presentColor = "#c8e6c9";
            _absentColor = "#ffbbaa";
            _unspecifiedColor = "#a5c7ff";
            _disabledColor = "#efefef";
            _baseColor = "#dcdcdc";
        }
    }
}
