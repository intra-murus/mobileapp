﻿namespace IntraMurus.Views.Converters
{
    public class StatusToTextColorConverter : StatusToColorConverter
    {
        public StatusToTextColorConverter()
        {
            _presentColor = "#4caf50";
            _absentColor = "#f44336";
            _unspecifiedColor = "#0c69ff";
            _disabledColor = "#aaaaaa";
            _baseColor = "#555555";
        }
    }
}
