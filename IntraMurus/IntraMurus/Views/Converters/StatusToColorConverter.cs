﻿using IntraMurus.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace IntraMurus.Views.Converters
{
    public class StatusToColorConverter : IValueConverter
    {
        protected string _presentColor;
        protected string _absentColor;
        protected string _unspecifiedColor;
        protected string _disabledColor;
        protected string _baseColor;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Availability availibility)
            {
                if (parameter is Availability expectedAvailability && availibility == expectedAvailability)
                {
                    switch (availibility)
                    {
                        case Availability.Present: return _presentColor;
                        case Availability.Absent: return _absentColor;
                        case Availability.Unspecified: return _unspecifiedColor;
                    }
                }
                else if (availibility == Availability.None)
                {
                    return _disabledColor;
                }
                return _baseColor;
            }

            if (value is Approbation approbation)
            {
                if (parameter is Approbation expectedApprobation && approbation == expectedApprobation)
                {
                    return _presentColor;
                }
                else
                {
                    return _disabledColor;
                }
            }

            throw new ArgumentException("Value is not valid type");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
