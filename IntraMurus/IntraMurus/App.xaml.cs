using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using IntraMurus.Views;
using IntraMurus.ViewModels.Base;
using IntraMurus.Models;
using IntraMurus.Services.Interfaces;
using IntraMurus.Services.Mocks;
using IntraMurus.Services;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace IntraMurus
{
	public partial class App : Application
	{
        private const string _userDetails = "UserDetails";

        private IMemoryCacheService _memoryCacheService;

        public static NavigationPage NavPage = null;

        public UserDetails UserDetails
        {
            get { return _memoryCacheService.Get<UserDetails>(_userDetails); }
            set { _memoryCacheService.Set(_userDetails, value); }
        }

        public App()
		{
			InitializeComponent();
            ViewModelLocator.Initialize();
            _memoryCacheService = ViewModelLocator.Resolve<IMemoryCacheService>();
            MainPage = new AuthentificationPage();
        }

		protected override void OnStart()
		{
            // Handle when your app starts
        }

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
