﻿using System;

namespace IntraMurus.Helpers
{
    public static class DateTimeFormatHelper
    {
        private enum Months
        {
            Janvier = 1,
            Février = 2,
            Mars = 3,
            Avril = 4,
            Mai = 5,
            Juin = 6,
            Juillet = 7,
            Août = 8,
            Septembre = 9,
            Octobre = 10,
            Novembre = 11,
            Décembre = 12
        };

        private enum Day
        {
            Dimanche = 0,
            Lundi = 1,
            Mardi = 2,
            Mercredi = 3,
            Jeudi = 4,
            Vendredi = 5,
            Samedi = 6
        };

        public static string GetDayOfWeek(DateTime date)
        {
            return ((Day)date.DayOfWeek).ToString();
        }

        public static string GetFormatedDate(DateTime date)
        {
            var month = ((Months)date.Month).ToString().ToLower();
            return string.Format($"{date.Day} {month} {date.Year}");
        }

        public static string GetFormatedMonthOfYear(DateTime date)
        {
            var month = ((Months)date.Month).ToString().ToUpper();
            return string.Format($"{month} {date.Year}");
        }

        public static string GetFormatedTimeSpan(DateTime date, TimeSpan timeSpan)
        {
            var startTime = date.TimeOfDay;
            var endTime = startTime + timeSpan;

            var startFormat = startTime.Hours < 10 ? "h':'mm" : "hh':'mm";
            var endFormat = endTime.Hours < 10 ? "h':'mm" : "hh':'mm";

            return $"{startTime.ToString(startFormat)} - {endTime.ToString(endFormat)}";
        }
    }
}
