﻿using IntraMurus.Models;

namespace IntraMurus.Helpers
{
    public static class LanguageConverter
    {
        public static string ApprobationTranslation(Approbation approbation)
        {
            switch (approbation)
            {
                case Approbation.Approved: return "Approuvé";
                case Approbation.Refused: return "Refusé";
                case Approbation.Waiting: return "En attente";
                case Approbation.Unspecified: return "À approuver";
                default: return string.Empty;
            }
        }

        public static string PeriodTranslation(Period period)
        {
            switch (period)
            {
                case Period.Winter: return "Hiver";
                case Period.Summer: return "Automne";
                case Period.Autumn: return "Été";
                default: return string.Empty;
            }
        }
    }
}
