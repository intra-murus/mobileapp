﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntraMurus.Helpers
{
    /// <summary>
    /// This class allows the execution of a single task at a time, making sure to first cancel the previous running task.
    /// If multiple tasks are queued, only the last one queued will actually execute. It is also possible to cancel the running task.
    /// </summary>

    public class UniqueExecutionTaskQueue
    {
        private readonly Queue<(Func<CancellationToken, Task>, CancellationToken)> _executionQueue;
        private readonly SemaphoreSlim _executionLock;
        private readonly SemaphoreSlim _cancellationLock;
        private readonly SemaphoreSlim _singleExecutionLock;

        private CancellationTokenSource _cancellationTokenSource;

        public UniqueExecutionTaskQueue()
        {
            _executionQueue = new Queue<(Func<CancellationToken, Task>, CancellationToken)>();
            _executionLock = new SemaphoreSlim(1, 1);
            _cancellationLock = new SemaphoreSlim(1, 1);
            _singleExecutionLock = new SemaphoreSlim(1, 1);
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public async Task Execute(Func<CancellationToken, Task> scheduleFunc, bool cancelRunningTask = true)
        {
            if (scheduleFunc == null)
                return;

            if (!cancelRunningTask)
                await _singleExecutionLock.WaitAsync();

            try
            {
                await _cancellationLock.WaitAsync();

                try
                {
                    _cancellationTokenSource.Cancel();
                    _cancellationTokenSource = new CancellationTokenSource();
                    _executionQueue.Enqueue((scheduleFunc, _cancellationTokenSource.Token));
                }
                finally
                {
                    _cancellationLock.Release();
                }

                await _executionLock.WaitAsync();

                try
                {
                    var (func, token) = _executionQueue.Dequeue();
                    await func(token);
                }
                finally
                {
                    _executionLock.Release();
                }
            }
            finally
            {
                if (!cancelRunningTask)
                    _singleExecutionLock.Release();
            }
        }

        public async Task Cancel()
        {
            await _cancellationLock.WaitAsync();

            try
            {
                _cancellationTokenSource.Cancel();
            }
            finally
            {
                _cancellationLock.Release();
            }
        }

        public bool IsPending()
        {
            return _executionQueue.Count > 0;
        }
    }
}
