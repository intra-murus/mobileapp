﻿using System;

namespace IntraMurus.Helpers
{
    public static class Extentions
    {
        public static DateTime ToUniversalTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTime);
        }

        public static long ToUnixTimeMilliseconds(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return date.AddTicks(-epoch.Ticks).Ticks / TimeSpan.TicksPerMillisecond;
        }

        public static long ToUnixTimeSeconds(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return date.AddTicks(-epoch.Ticks).Ticks / TimeSpan.TicksPerSecond;
        }
    }
}
