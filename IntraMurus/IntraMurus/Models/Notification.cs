﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntraMurus.Models
{
    public class Notification
    {
       public NotificationType NotificationEnumType { get; set; }

       public string SentMessage { get; set; }

       public string Sender { get; set; }

       public int IdSender { get; set; }
    }
}
