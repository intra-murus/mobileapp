﻿using System;
using System.Collections.Generic;

namespace IntraMurus.Models
{
    public class Activity : IComparable<Activity>
    {
        public int ActivityId { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan Duration { get; set; }

        public string Location { get; set; }

        public int TeamId { get; set; }

        public string TeamName { get; set; }

        public string TeamCaptainCip { get; set; }

        public Availability Status { get; set; }

        public Availability TeamStatus { get; set; }

        public DateTime ConfirmationLimit { get; set; }

        public IEnumerable<Player> Players { get; set; }

        public int CompareTo(Activity other)
        {
            if (!Date.Equals(other.Date))
            {
                return Date.CompareTo(other.Date);
            } 
            else
            {
                return Duration.CompareTo(other.Duration);
            }
        }
    }
}
