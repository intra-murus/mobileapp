﻿using System;
using System.Collections.Generic;

namespace IntraMurus.Models
{
    public class Inscription
    {
        public string SportName { get; set; }

        public string Category { get; set; }

        public ICollection<Player> PlayersList { get; set; }

        public string TeamName { get; set; }

        public Period Period { get; set; }

        public int Year { get; set; }
    }
}
