﻿namespace IntraMurus.Models
{
    public enum Availability
    {
        None,
        Unspecified,
        Present,
        Absent,
        Cancelled
    }
}
