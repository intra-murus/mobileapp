﻿namespace IntraMurus.Models
{
    public enum Approbation
    {
        Invalid,
        Unspecified,
        Waiting,
        Approved,
        Refused
    }
}
