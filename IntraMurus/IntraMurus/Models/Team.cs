﻿using System;

namespace IntraMurus.Models
{
    public class Team : IComparable<Team>
    {
        public int TeamId { get; set; }

        public string CipCapitaine {get; set;}

        public string NomEquipe { get; set; }

        public Inscription InscriptionData { get; set; }

        public Approbation Status { get; set; }

        public int CompareTo(Team other)
        {
            if (!Status.Equals(other.Status))
            {
                return Status.CompareTo(other.Status);
            }
            else
            {
                return TeamId.CompareTo(other.TeamId);
            }
        }
    }
}
