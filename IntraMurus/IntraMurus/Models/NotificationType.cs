﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntraMurus.Models
{
    public enum NotificationType
    {
        Message,
        Invitation,
        Unspecified
    }
}
