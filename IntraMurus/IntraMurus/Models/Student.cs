﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntraMurus.Models
{
    [Serializable]
    public class Student
    {
        public string App { get; set; }
        public string Cip_etudiant { get; set; }
        public double Cote_r { get; set; }
        public string Departement { get; set; }
        public string Inscription { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Profil_id { get; set; }
        public string Programme { get; set; }
        public string Trimestre_id { get; set; }
        public string Unit_id { get; set; }

    }
}
