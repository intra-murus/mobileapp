﻿namespace IntraMurus.Models
{
    public class Player
    {
        public string Name { get; set; }

        public Availability Status { get; set; }

        public Confirmation ConfirmationStatus { get; set; }

        public string CIP { get; set; }

        public int TeamId { get; set; }
    }
}
