﻿namespace IntraMurus.Models
{
    public enum Confirmation
    {
        Invalid,
        Unspecified,
        Confirmed,
        Cancelled
    }
}
