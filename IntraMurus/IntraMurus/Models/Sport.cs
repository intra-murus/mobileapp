﻿using System.Collections.Generic;

namespace IntraMurus.Models
{
    public class Sport
    { 
        public string Name { get; set; }

        public List<string> CategoriesList { get; set; }
    }
}
