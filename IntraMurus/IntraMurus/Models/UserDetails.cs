﻿namespace IntraMurus.Models
{
    public class UserDetails
    {
        public string Cip { get; set; }

        public string Email { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }
    }
}
