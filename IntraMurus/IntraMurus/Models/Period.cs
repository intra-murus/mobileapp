﻿namespace IntraMurus.Models
{
    public enum Period
    {
        Invalid,
        Winter,
        Summer,
        Autumn
    }
}
