﻿namespace IntraMurus.Network.Response
{
    public class UserTeamResponse : BaseResponse
    {
      public int annee { get; set; }

      public string cipCapitaine { get; set; }

      public int idEquipe { get; set; }

      public string nomEquipe { get; set; }

      public string nomLigue { get; set; }

      public string periode { get; set; }

      public string sport { get; set; }

      public string statutApprobation { get; set; }
    }
}
