﻿namespace IntraMurus.Network.Response
{
    public class BaseResponse
    {
        public ApiStatus status { get; set; }

        public ulong timestamp { get; set; }

        public string message { get; set; }

        public string details { get; set; }
    }
}
