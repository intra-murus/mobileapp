﻿namespace IntraMurus.Network.Response
{
    public class ScheduleResponse : BaseResponse
    {
        public int idMatch { get; set; }

        public string sport { get; set; }

        public string nomLigue { get; set; }

        public string nomEquipe { get; set; }

        public string terrain { get; set; }

        public string statutMatch { get; set; }

        public string statutMembreMatch { get; set; }

        public long dateDebut { get; set; }

        public long dateFin { get; set; }
    }
}
