﻿namespace IntraMurus.Network.Response
{
    public class AuthentificationResponse : BaseResponse
    {
        public string token { get; set; }
    }
}
