﻿namespace IntraMurus.Network.Response
{
    public class DetailedActivityResponse : ScheduleResponse
    {
        public string periode { get; set; }

        public int annee { get; set; }

        public string arbitre { get; set; }

        public int idEquipe { get; set; }

        public string cipCapitaine { get; set; }

        public string statutEquipeMatch { get; set; }

        public string cote { get; set; }
    }
}
