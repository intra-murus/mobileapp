﻿using System;

namespace IntraMurus.Network.Response
{
    public class UserDetailsResponse : BaseResponse
    {
        public string cip { get; set; }

        public string email { get; set; }

        public string nom { get; set; }

        public string prenom { get; set; }
    }
}
