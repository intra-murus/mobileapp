﻿namespace IntraMurus.Network.Response
{
    public class MessageResponse : BaseResponse
    {
        public int IdMessage { get; set; }
        public int Lien { get; set; }
        public string Text { get; set; }
        public string TypeMessage { get; set; }
    }
}
