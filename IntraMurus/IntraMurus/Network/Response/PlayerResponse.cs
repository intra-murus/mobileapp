﻿
namespace IntraMurus.Network.Response
{
    public class PlayerResponse : BaseResponse
    {
        public string cip { get; set; }

        public int idEquipe { get; set; }

        public int idMatch { get; set; }

        public string nom { get; set; }

        public string prenom { get; set; }

        public string statutMembreMatch { get; set; }

        public string statutMembreEquipe { get; set; }
    }
}
