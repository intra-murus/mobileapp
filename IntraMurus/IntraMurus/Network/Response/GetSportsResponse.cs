﻿using System.Collections;
using System.Collections.Generic;

namespace IntraMurus.Network.Response
{
    public class GetSportsResponse : BaseResponse, IEnumerable<string>
    {
        private List<string> _list = new List<string>();

        public IEnumerator<string> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
}
