﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntraMurus.Network.Request
{
    public class PutStatusTeamMember
    {
        public string cip { get; set; }

        public int idEquipe { get; set; }

        public string statutMembreEquipe { get; set; }
    }
}
