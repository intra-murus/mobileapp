﻿namespace IntraMurus.Network.Request
{
    public class PutTeamStatusRequest
    {
        public int idEquipe { get; set; }

        public int idMatch { get; set; }

        public string statutEquipeMatch { get; set; }
    }
}
