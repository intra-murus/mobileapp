﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntraMurus.Network.Request
{
    public class PutStatusApprobationRequest
    {
        public int idEquipe { get; set; }

        public string statutApprobation { get; set; }
    }
}
