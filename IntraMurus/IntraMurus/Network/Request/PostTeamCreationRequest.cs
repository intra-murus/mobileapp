﻿namespace IntraMurus.Network.Request
{
    public class PostTeamCreationRequest
    {
        public int annee { get; set; }

        public string cipCapitaine { get; set; }

        public string cipsJoueurs { get; set; }

        public string nomLigue { get; set; }

        public int idLigue { get; set; }

        public string nomEquipe { get; set; }

        public string periode { get; set; }

        public string sport { get; set; }
    }
}
