﻿namespace IntraMurus.Network.Request
{
    public class PutPlayerStatusRequest
    {
        public int idMatch { get; set; }

        public string statutMembreMatch { get; set; }
    }
}
