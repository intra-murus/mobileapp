﻿namespace IntraMurus.Network.Helpers
{
    public interface IWebDataCleaner
    {
        void ClearData();
    }
}
