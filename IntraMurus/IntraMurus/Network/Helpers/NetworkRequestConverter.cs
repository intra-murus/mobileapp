﻿using IntraMurus.Models;
using IntraMurus.Network.Request;

namespace IntraMurus.Network.Helpers
{
    public static class NetworkRequestConverter
    {
        public static PutPlayerStatusRequest ConvertActivityToPutPlayerStatusRequest(Activity activity)
        {
            return new PutPlayerStatusRequest
            {
                idMatch = activity.ActivityId,
                statutMembreMatch = GetAvailability(activity.Status)
            };
        }

        public static PutTeamStatusRequest ConvertActivityToPutTeamStatusRequest(Activity activity)
        {
            return new PutTeamStatusRequest
            {
                idMatch = activity.ActivityId,
                idEquipe = activity.TeamId,
                statutEquipeMatch = GetAvailability(activity.TeamStatus)
            };
        }

        private static string GetAvailability(Availability status)
        {
            switch (status)
            {
                case Availability.Present: return "PRESENT";
                case Availability.Absent: return "ABSENT";
                case Availability.Unspecified: return "EN_ATTENTE";
                default: return "EN_ATTENTE";
            }
        }
    }
}
