﻿using IntraMurus.Helpers;
using IntraMurus.Models;
using IntraMurus.Network.Response;
using System;
using System.Globalization;

namespace IntraMurus.Network.Helpers
{
    public static class NetworkResponseConverter
    {
        static readonly TimeSpan NumberOfDaysToConfirm = new TimeSpan(3, 0, 0, 0);
        static readonly TimeSpan TimeBeforeActivityToConfirm = new TimeSpan(1, 0, 0);

        public static Activity ConvertScheduleResponseToActivity(ScheduleResponse response)
        {
            var date = response.dateDebut.ToUniversalTime();
            var confirmationStarted = date - NumberOfDaysToConfirm < DateTime.UtcNow;

            return new Activity
            {
                ActivityId = response.idMatch,
                Name = $"{GetPrettyString(response.sport)} {response.nomLigue}",
                Date = date.ToLocalTime(),
                Duration = response.dateFin.ToUniversalTime() - date,
                Location = GetPrettyString(response.terrain),
                TeamName = GetPrettyString(response.nomEquipe),
                Status = confirmationStarted ? GetAvailability(response.statutMembreMatch) : Availability.None,
                ConfirmationLimit = confirmationStarted ? date.ToLocalTime() - TimeBeforeActivityToConfirm : default(DateTime)
            };
        }

        public static Player ConvertPlayerResponseToPlayer(PlayerResponse response)
        {
            return new Player
            {
                CIP = response.cip,
                Name = $"{response.prenom} {response.nom}",
                Status = GetAvailability(response.statutMembreMatch),
                TeamId = response.idEquipe,
                ConfirmationStatus = GetConfirmation(response.statutMembreEquipe)
            };
        }

        public static Activity ConvertDetailedActivityResponseToActivity(DetailedActivityResponse response)
        {
            var activity = ConvertScheduleResponseToActivity(response);

            activity.TeamId = response.idEquipe;
            activity.TeamStatus = GetAvailability(response.statutEquipeMatch);
            activity.TeamCaptainCip = response.cipCapitaine;

            return activity;
        }

        public static UserDetails ConvertUserDetailsResponseToUserDetails(UserDetailsResponse response)
        {
            return new UserDetails
            {
                Cip = response.cip,
                Email = response.email,
                Nom = response.nom,
                Prenom = response.prenom
            };
        }

        public static Team ConvertUserTeamResponseToUserTeam(UserTeamResponse response)
        {
            return new Team
            {
                TeamId = response.idEquipe,
                CipCapitaine = response.cipCapitaine,
                NomEquipe = response.nomEquipe,
                Status = GetApprobation(response.statutApprobation),
                InscriptionData = new Inscription
                {
                    TeamName = GetPrettyString(response.nomEquipe),
                    SportName = GetPrettyString(response.sport),
                    Category = response.nomLigue,
                    Year = response.annee,
                    Period = GetPeriod(response.periode)
                }
            };
        }

        private static string GetPrettyString(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.Replace('_', ' ').ToLower());
        }

        private static Availability GetAvailability(string status)
        {
            switch (status?.ToLower())
            {
                case "present": return Availability.Present;
                case "absent": return Availability.Absent;
                case "en_attente": return Availability.Unspecified;
                default: return Availability.Unspecified;
            }
        }

        public static Notification ConvertMessageResponseToNotification(MessageResponse response, string IdString)
        {
            return new Notification
            {
                NotificationEnumType = GetNotificationType(response.TypeMessage),
                SentMessage = response.Text,
                IdSender = response.Lien,
                Sender = IdString
            };
        }

        private static NotificationType GetNotificationType(string messageType)
        {
            switch (messageType)
            {
                case "TEXT": return NotificationType.Message;
                case "INVITATION": return NotificationType.Invitation;
                default: return NotificationType.Unspecified;
            }
        }

        private static Approbation GetApprobation(string approbation)
        {
            switch (approbation?.ToLower())
            {
                case "approuve": return Approbation.Approved;
                case "refuse": return Approbation.Refused;
                case "en_attente": return Approbation.Unspecified;
                case "pret_a_approuver": return Approbation.Waiting;
                default: return Approbation.Invalid;
            }
        }

        private static Confirmation GetConfirmation(string confirmation)
        {
            switch (confirmation?.ToLower())
            {
                case "accepte": return Confirmation.Confirmed;
                case "refuse": return Confirmation.Cancelled;
                case "en_attente": return Confirmation.Unspecified;
                default: return Confirmation.Invalid;
            }
        }

        private static Period GetPeriod(string period)
        {
            switch (period?.ToLower())
            {
                case "automne": return Period.Autumn;
                case "hiver": return Period.Summer;
                case "ete": return Period.Winter;
                default: return Period.Invalid;
            }
        }
    }
}
