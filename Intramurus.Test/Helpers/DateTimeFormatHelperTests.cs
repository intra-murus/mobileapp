﻿using IntraMurus.Helpers;
using System;
using Xunit;

namespace Intramurus.Test.Helpers
{
    public class DateTimeFormatHelperTests
    {
        private static readonly DateTime ADate = new DateTime(2018, 10, 24);
        private static readonly string ADayOfWeek = "Mercredi";
        private static readonly string AFormatedDate = "24 octobre 2018";
        private static readonly string AFormatedMonthOfYear = "OCTOBRE 2018";
        private static readonly TimeSpan ATimeSpan = new TimeSpan(1, 30, 0);
        private static readonly string AFormatedTimeSpan = "0:00 - 1:30";
        private static readonly string AFormatedTimeSpanOnMultipleDays = "23:30 - 1:00";

        [Fact]
        public void TestGetDayOfWeek()
        {
            // Arrange

            // Act
            var dayOfWeek = DateTimeFormatHelper.GetDayOfWeek(ADate);

            // Assert
            Assert.Equal(ADayOfWeek, dayOfWeek);
        }

        [Fact]
        public void TestGetFormatedDate()
        {
            // Arrange

            // Act
            var formatedDate = DateTimeFormatHelper.GetFormatedDate(ADate);

            // Assert
            Assert.Equal(AFormatedDate, formatedDate);
        }

        [Fact]
        public void TestGetFormatedMonthOfYear()
        {
            // Arrange

            // Act
            var formatedMonthOfYear = DateTimeFormatHelper.GetFormatedMonthOfYear(ADate);

            // Assert
            Assert.Equal(AFormatedMonthOfYear, formatedMonthOfYear);
        }

        [Fact]
        public void TestGetFormatedTimeSpan_TimeSpanEndInsideCurrentDate()
        {
            // Arrange

            // Act
            var formatedTimeSpan = DateTimeFormatHelper.GetFormatedTimeSpan(ADate, ATimeSpan);

            // Assert
            Assert.Equal(AFormatedTimeSpan, formatedTimeSpan);
        }

        [Fact]
        public void TestGetFormatedTimeSpan_TimeSpanEndOutsideCurrentDate()
        {
            // Arrange
            var dateWithLateTime = ADate.Add(new TimeSpan(23, 30, 0));

            // Act
            var formatedTimeSpan = DateTimeFormatHelper.GetFormatedTimeSpan(dateWithLateTime, ATimeSpan);

            // Assert
            Assert.Equal(AFormatedTimeSpanOnMultipleDays, formatedTimeSpan);
        }
    }
}
